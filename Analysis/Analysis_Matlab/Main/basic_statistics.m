clear all

resFolder = '/Users/polinaarbuzova/Documents/TS study/Results';
cd(resFolder);

%load('hc_hist_cprime_MLE_quant.mat');
load('ts_hist_cprime_MLE_quant.mat');
sample_size = length(subjectNames);


%for vision
for i=1:sample_size
    if SSEfitAll(i).vision.da ~= Inf
        vis.da(i) = SSEfitAll(i).vision.da;
        vis.meta_da(i) = SSEfitAll(i).vision.meta_da;
        vis.mratio(i) = SSEfitAll(i).vision.M_ratio;
    else
        vis.da(i) = NaN;
        vis.meta_da(i) = NaN;
        vis.mratio(i) = NaN;        
    end
    vis.perc_corr(i) = corrPercAll(i).vision;
    vis.twoBin_HR2_rS1(i) = SSEfit_2binsAll(i).vision.HR2_rS1;
    vis.twoBin_FAR2_rS1(i) = SSEfit_2binsAll(i).vision.FAR2_rS1;
    vis.twoBin_HR2_rS2(i) = SSEfit_2binsAll(i).vision.HR2_rS2;
    vis.twoBin_FAR2_rS2(i) = SSEfit_2binsAll(i).vision.FAR2_rS2;
    % Confidence
    vis.confMean(i) = confMeanAll(i).vision;
    vis.confMed(i) = confMedianAll(i).vision;
    
    if MLEfitAll(i).vision.da ~= Inf
        vis.da_MLE(i) = MLEfitAll(i).vision.da;
        vis.meta_da_MLE(i) = MLEfitAll(i).vision.meta_da;
        vis.mratio_MLE(i) = MLEfitAll(i).vision.M_ratio;
    else
        vis.da_MLE(i) = NaN;
        vis.meta_da_MLE(i) = NaN;
        vis.mratio_MLE(i) = NaN;
    end

    
    %%% Quantile binning
    if SSEfit_q(i).vision.da ~= Inf
        vis_q.da(i) = SSEfit_q(i).vision.da;
        vis_q.meta_da(i) = SSEfit_q(i).vision.meta_da;
        vis_q.mratio(i) = SSEfit_q(i).vision.M_ratio;
    else
        vis_q.da(i) = NaN;
        vis_q.meta_da(i) = NaN;
        vis_q.mratio(i) = NaN;        
    end
    vis_q.perc_corr(i) = corrPercAll(i).vision;
    vis_q.twoBin_HR2_rS1(i) = SSEfit_2bins_q(i).vision.HR2_rS1;
    vis_q.twoBin_FAR2_rS1(i) = SSEfit_2bins_q(i).vision.FAR2_rS1;
    vis_q.twoBin_HR2_rS2(i) = SSEfit_2bins_q(i).vision.HR2_rS2;
    vis_q.twoBin_FAR2_rS2(i) = SSEfit_2bins_q(i).vision.FAR2_rS2;
    
    % Confidence
    vis_q.confMean(i) = confMeanAll(i).vision;
    vis_q.confMed(i) = confMedianAll(i).vision;
    
    if MLEfit_q(i).vision.da ~= Inf
        vis_q_mle.da_MLE(i) = MLEfit_q(i).vision.da;
        vis_q_mle.meta_da_MLE(i) = MLEfit_q(i).vision.meta_da;
        vis_q_mle.mratio_MLE(i) = MLEfit_q(i).vision.M_ratio;
    else
        vis_q_mle.da_MLE(i) = NaN;
        vis_q_mle.meta_da_MLE(i) = NaN;
        vis_q_mle.mratio_MLE(i) = NaN;
    end

    
    
end

%scatter(vis.mratio, vis.mratio_MLE)


%for touch
for i=1:sample_size
    if SSEfitAll(i).touch.da ~= Inf
        touch.da(i) = SSEfitAll(i).touch.da;
        touch.meta_da(i) = SSEfitAll(i).touch.meta_da;
        touch.mratio(i) = SSEfitAll(i).touch.M_ratio;
    else
        touch.da(i) = NaN;
        touch.meta_da(i) = NaN;
        touch.mratio(i) = NaN;
    end
    touch.perc_corr(i) = corrPercAll(i).touch;
    touch.twoBin_HR2_rS1(i) = SSEfit_2binsAll(i).touch.HR2_rS1;
    touch.twoBin_FAR2_rS1(i) = SSEfit_2binsAll(i).touch.FAR2_rS1;
    touch.twoBin_HR2_rS2(i) = SSEfit_2binsAll(i).touch.HR2_rS2;
    touch.twoBin_FAR2_rS2(i) = SSEfit_2binsAll(i).touch.FAR2_rS2;  
    % Confidence
    touch.confMean(i) = confMeanAll(i).touch;
    touch.confMed(i) = confMedianAll(i).touch;
    
    
    if MLEfitAll(i).touch.da ~= Inf
        touch.da_MLE(i) = MLEfitAll(i).touch.da;
        touch.meta_da_MLE(i) = MLEfitAll(i).touch.meta_da;
        touch.mratio_MLE(i) = MLEfitAll(i).touch.M_ratio;
    else
        touch.da_MLE(i) = NaN;
        touch.meta_da_MLE(i) = NaN;
        touch.mratio_MLE(i) = NaN;
    end
    %vis.twoBin_MLE_HR2_rS1(i) = MLEfit_2binsAll(i).vision.HR2_rS1;
    %vis.twoBin_MLE_FAR2_rS1(i) = MLEfit_2binsAll(i).vision.FAR2_rS1;
    %vis.twoBin_MLE_HR2_rS2(i) = MLEfit_2binsAll(i).vision.HR2_rS2;
    %vis.twoBin_MLE_FAR2_rS2(i) = MLEfit_2binsAll(i).vision.FAR2_rS2;
    
    %%% Quantile binning
    if SSEfit_q(i).vision.da ~= Inf
        touch_q.da(i) = SSEfit_q(i).touch.da;
        touch_q.meta_da(i) = SSEfit_q(i).touch.meta_da;
        touch_q.mratio(i) = SSEfit_q(i).touch.M_ratio;
    else
        touch_q.da(i) = NaN;
        touch_q.meta_da(i) = NaN;
        touch_q.mratio(i) = NaN;        
    end
    touch_q.perc_corr(i) = corrPercAll(i).touch;
    touch_q.twoBin_HR2_rS1(i) = SSEfit_2bins_q(i).touch.HR2_rS1;
    touch_q.twoBin_FAR2_rS1(i) = SSEfit_2bins_q(i).touch.FAR2_rS1;
    touch_q.twoBin_HR2_rS2(i) = SSEfit_2bins_q(i).touch.HR2_rS2;
    touch_q.twoBin_FAR2_rS2(i) = SSEfit_2bins_q(i).touch.FAR2_rS2;
    
    
    if MLEfit_q(i).vision.da ~= Inf
        touch_q_mle.da_MLE(i) = MLEfit_q(i).touch.da;
        touch_q_mle.meta_da_MLE(i) = MLEfit_q(i).touch.meta_da;
        touch_q_mle.mratio_MLE(i) = MLEfit_q(i).touch.M_ratio;
    else
        touch_q_mle.da_MLE(i) = NaN;
        touch_q_mle.meta_da_MLE(i) = NaN;
        touch_q_mle.mratio_MLE(i) = NaN;
    end
end

subj_ids = [1:1:sample_size];
figure
scatter([subj_ids], [touch.mratio], 'filled')
hold on
scatter([subj_ids], [touch.mratio_MLE], 'filled')
hold on
scatter([subj_ids], [vis.mratio], 'filled')
hold on
scatter([subj_ids], [vis.mratio_MLE], 'filled')

figure
scatter([subj_ids], [vis.mratio-vis.mratio_MLE], 'filled')
hold on
scatter([subj_ids], [touch.mratio-touch.mratio_MLE], 'filled')
ylim([-0.5 0.5])

nanmean(vis.mratio)
nanmean(vis.mratio_MLE)
nanmean(touch.mratio)
nanmean(touch.mratio_MLE)

% % Get all the m-ratio nice and neat:
% for i = 1:sample_size
%     % vision
%     mratio_vision(i) = SDTres{i}{1}.M_ratio
%     % touch
%     mratio_touch(i) = SDTres{i}{2}.M_ratio
% end

% Exclude based on the performance 
lower_boundary = 60;
upper_boundary = 85;

for i = 1:sample_size
    if vis.perc_corr(i) < lower_boundary || vis.perc_corr(i) > upper_boundary
        vis.da(i) = NaN;
        vis.meta_da(i) = NaN;
        vis.mratio(i) = NaN;
        % Confidence
        vis.confMean(i) = NaN;
        vis.confMed(i) = NaN;
      
        % MLE
        vis.da_MLE(i) = NaN;
        vis.meta_da_MLE(i) = NaN;
        vis.mratio_MLE(i) = NaN;
        
        %%%%% Quantile binning %%%%%
        % SSE
        vis_q.mratio(i) = NaN;
                
        % MLE
        vis_q_MLE.mratio(i) = NaN;
        
        fprintf('performance outside boundaries in visual condition for subject: %s\n', subjectNames{i});
    end
    
    if touch.perc_corr(i) < lower_boundary || touch.perc_corr(i) > upper_boundary
        touch.da(i) = NaN;
        touch.meta_da(i) = NaN;
        touch.mratio(i) = NaN;
        
        % Confidence
        touch.confMean(i) = NaN;
        touch.confMed(i) = NaN;
        
        % MLE
        touch.da_MLE(i) = NaN;
        touch.meta_da_MLE(i) = NaN;
        touch.mratio_MLE(i) = NaN;
        
        %%%%% Quantile binning %%%%%
        % SSE
        touch_q.mratio(i) = NaN;
                
        % MLE
        touch_q_mle.mratio(i) = NaN;
        
        fprintf('performance outside boundaries in touch condition for subject: %s\n', subjectNames{i});    
    end
    
end

% Exclude based on SDT based measures
% There is NaNs in cases where FAR2 and HR2 was <0.025 or >0.975 or when d_1 could not be estimated (d_1 = Inf, this happens in step1_describeSkittles_blindNonBlind) or when performance was
% below 60 % or above 85 %.
lower_boundary = 0.025;
upper_boundary = 0.975;

for i = 1:sample_size
    if vis.twoBin_HR2_rS1(i) < lower_boundary || vis.twoBin_HR2_rS1(i) > upper_boundary || vis.twoBin_FAR2_rS1(i) < lower_boundary || vis.twoBin_FAR2_rS1(i) > upper_boundary || vis.twoBin_HR2_rS2(i) < lower_boundary || vis.twoBin_HR2_rS2(i) > upper_boundary || vis.twoBin_FAR2_rS2(i) < lower_boundary || vis.twoBin_FAR2_rS2(i) > upper_boundary 
        vis.da(i) = NaN;
        vis.meta_da(i) = NaN;
        vis.mratio(i) = NaN;
        
        % MLE
        vis.da_MLE(i) = NaN;
        vis.meta_da_MLE(i) = NaN;
        vis.mratio_MLE(i) = NaN;
        
        % Confidence
        vis.confMean(i) = NaN;
        vis.confMed(i) = NaN;
        fprintf('SDT measures outside boundaries in visual condition for subject: %s\n', subjectNames{i});
    end
    
    if touch.twoBin_HR2_rS1(i) < lower_boundary || touch.twoBin_HR2_rS1(i) > upper_boundary || touch.twoBin_FAR2_rS1(i) < lower_boundary || touch.twoBin_FAR2_rS1(i) > upper_boundary || touch.twoBin_HR2_rS2(i) < lower_boundary || touch.twoBin_HR2_rS2(i) > upper_boundary || touch.twoBin_FAR2_rS2(i) < lower_boundary || touch.twoBin_FAR2_rS2(i) > upper_boundary
        touch.da(i) = NaN;
        touch.meta_da(i) = NaN;
        touch.mratio(i) = NaN;
        
        % MLE 
        touch.da(i) = NaN;
        touch.meta_da(i) = NaN;
        touch.mratio(i) = NaN;
        
        % Confidence
        touch.confMean(i) = NaN;
        touch.confMed(i) = NaN;
        fprintf('SDT measures outside boundaries in touch condition for subject: %s\n', subjectNames{i});    
    end
    
end

% Exclude based on too high skipped trials number
skipped_threshold = 0.13;

for i = 1:sample_size
    if exclSkipped(i).vision > skipped_threshold
        vis.da(i) = NaN;
        vis.meta_da(i) = NaN;
        vis.mratio(i) = NaN;
        % MLE
        vis.da_MLE(i) = NaN;
        vis.meta_da_MLE(i) = NaN;
        vis.mratio_MLE(i) = NaN;
        
        % Confidence
        vis.confMean(i) = NaN;
        vis.confMed(i) = NaN;
        
        %%%%% Quantile binning %%%%%
        % SSE
        vis_q.mratio(i) = NaN;
                
        % MLE
        vis_q_mle.mratio(i) = NaN;
        
        fprintf('Number of skipped trials too high in visual condition for subject: %s\n', subjectNames{i});
    end
    
    if exclSkipped(i).touch > skipped_threshold
        touch.da(i) = NaN;
        touch.meta_da(i) = NaN;
        touch.mratio(i) = NaN;
        % MLE
        touch.da_MLE(i) = NaN;
        touch.meta_da_MLE(i) = NaN;
        touch.mratio_MLE(i) = NaN;
        
        % Confidence
        touch.confMean(i) = NaN;
        touch.confMed(i) = NaN;
        
        %%%%% Quantile binning %%%%%
        % SSE
        touch_q.mratio(i) = NaN;
                
        % MLE
        touch_q_mle.mratio(i) = NaN;
        
        fprintf('Number of skipped trials too high in touch condition for subject: %s\n', subjectNames{i});    
    end
    
end


% % Find which participants have to be excluded because of the 
% idx_skipped_vis = find([exclSkipped.vision]<0.1)
% idx_skipped_touch = find([exclSkipped.touch]<0.1)
% Clean
idx_clean_vis = find(~isnan(vis.mratio));
idx_clean_touch = find(~isnan(touch.mratio));
idx_clean = intersect(idx_clean_vis, idx_clean_touch);
% MLE
idx_clean_vis_MLE = find(~isnan(vis.mratio_MLE));
idx_clean_touch_MLE = find(~isnan(touch.mratio_MLE));
idx_clean_MLE = intersect(idx_clean_vis_MLE, idx_clean_touch_MLE);

%%% Quantile binning
idx_clean_vis_q = find(~isnan(vis_q.mratio)); 
idx_clean_touch_q = find(~isnan(touch_q.mratio));
idx_clean_q = intersect(idx_clean_vis_q, idx_clean_touch_q);
% MLE
idx_clean_vis_q_MLE = find(~isnan(vis_q_mle.mratio_MLE)); 
idx_clean_touch_q_MLE = find(~isnan(touch_q_mle.mratio_MLE));
idx_clean_q_MLE = intersect(idx_clean_vis_q_MLE, idx_clean_touch_q_MLE);


%[h,p,ci,stats] = ttest(vis.mratio(idx_clean), touch.mratio(idx_clean))
%robust_correlation(vis.mratio(idx_clean), touch.mratio(idx_clean))

% Save
save('ts_hist_mratios_rt_exl13_MLE_quant.mat', 'touch', 'vis', 'touch_q', 'touch_q_mle', 'vis_q', 'vis_q_mle', 'idx_clean', 'idx_clean_vis', 'idx_clean_touch', 'idx_clean_MLE', 'idx_clean_vis_MLE', 'idx_clean_touch_MLE', 'idx_clean_vis_q', 'idx_clean_touch_q', 'idx_clean_q', 'idx_clean_vis_q_MLE', 'idx_clean_touch_q_MLE', 'idx_clean_q_MLE');
