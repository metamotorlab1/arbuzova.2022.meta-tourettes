% Polina Arbuzova, Novemeber 2020

% Plot bin counts for TS study



close all
clear all

user = 'polina';

switch user
    case {'polina'}
        saveDir = '/Users/polinaarbuzova/Documents/TS study/Results';
        addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/HMeta-d-master'));
        saveBins = '/Users/polinaarbuzova/Documents/TS study/Analysis R/Bins counts';
end


cd(saveDir);

ts = load(['ts_hist_cprime_new_excl.mat'])
hc = load(['hc_hist_cprime_new_excl.mat'])

% Visual
figure
suptitle('VISUAL')
subplot(2,2,1)
for s=1:ts.i
    for p=1:length(ts.nR_S1(s).vision)
        plot(ts.nR_S1(s).vision)
        hold on
    end
end
title('TS, nR__S1')

%figure
subplot(2,2,2)
for s=1:ts.i
    for p=1:length(ts.nR_S2(s).vision)
        plot(ts.nR_S2(s).vision)
        hold on
    end
end
title('TS, nR__S2')

%figure
subplot(2,2,3)
for s=1:hc.i
    for p=1:length(hc.nR_S1(s).vision)
        plot(hc.nR_S1(s).vision)
        hold on
    end
end
title('HC, nR__S1')

%figure
subplot(2,2,4)
for s=1:hc.i
    for p=1:length(hc.nR_S2(s).vision)
        plot(hc.nR_S2(s).vision)
        hold on
    end
end
title('HC, nR__S2')

% Tactile

figure
suptitle('TACTILE')
subplot(2,2,1)
for s=1:ts.i
    for p=1:length(ts.nR_S1(s).touch)
        plot(ts.nR_S1(s).touch)
        hold on
    end
end
title('TS, nR__S1')

subplot(2,2,2)
for s=1:ts.i
    for p=1:length(ts.nR_S2(s).touch)
        plot(ts.nR_S2(s).touch)
        hold on
    end
end
title('TS, nR__S2')

subplot(2,2,3)
for s=1:hc.i
    for p=1:length(hc.nR_S1(s).touch)
        plot(hc.nR_S1(s).touch)
        hold on
    end
end
title('HC, nR__S1')

subplot(2,2,4)
for s=1:hc.i
    for p=1:length(hc.nR_S2(s).touch)
        plot(hc.nR_S2(s).touch)
        hold on
    end
end
title('HC, nR__S2')

% Export data for R
% Columns:
% group / task / nR_S1 / nR_S2
% TS
ts_vision_nrs1 = [ts.nR_S1.vision]';
ts_vision_nrs2 = [ts.nR_S2.vision]';
ts_group_vis = cell(length(ts_vision_nrs1), 1);
ts_group_vis(:) = {'TS'};
ts_task_vis = cell(length(ts_vision_nrs1), 1);
ts_task_vis(:) = {'visual'};

ts_touch_nrs1 = [ts.nR_S1.touch]';
ts_touch_nrs2 = [ts.nR_S2.touch]';
ts_group_touch = cell(length(ts_touch_nrs1), 1);
ts_group_touch(:) = {'TS'};
ts_task_touch = cell(length(ts_touch_nrs1), 1);
ts_task_touch(:) = {'tactile'};

% HC
hc_vision_nrs1 = [hc.nR_S1.vision]';
hc_vision_nrs2 = [hc.nR_S2.vision]';
hc_group_vis = cell(length(hc_vision_nrs1), 1);
hc_group_vis(:) = {'HC'};
hc_task_vis = cell(length(hc_vision_nrs1), 1);
hc_task_vis(:) = {'visual'};

hc_touch_nrs1 = [hc.nR_S1.touch]';
hc_touch_nrs2 = [hc.nR_S2.touch]';
hc_group_touch = cell(length(hc_touch_nrs1), 1);
hc_group_touch(:) = {'HC'};
hc_task_touch = cell(length(hc_touch_nrs1), 1);
hc_task_touch(:) = {'tactile'};

% Data frame
group = [hc_group_touch; hc_group_vis; ts_group_touch; ts_group_vis];
task = [hc_task_touch; hc_task_vis; ts_task_touch; ts_task_vis];
nrs1 = [hc_touch_nrs1; hc_vision_nrs1; ts_touch_nrs1; ts_vision_nrs1];
nrs2 = [hc_touch_nrs2; hc_vision_nrs2; ts_touch_nrs2; ts_vision_nrs2];

% Number of the bin
b = [1:1:12];
n_rep = length(nrs1)/12;
bins = repmat(b,1,n_rep)';

% Put together in a table
tsStudyBins = table(group, task, bins, nrs1, nrs2)
cd(saveBins);
writetable(tsStudyBins,'ts_study_bin_counts.csv') 
