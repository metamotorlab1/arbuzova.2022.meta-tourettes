
function [Info] = run_tactile_experiment(varargin)

clear all

clearvib=instrfind();
if ~isempty(clearvib)
    fclose(clearvib);
    delete(clearvib)
end

global cfg
global dev

realExperiment  = 0;                                                        %set to 1 to hide cursor, etc
cfg.novib = 0; % set to 1 for coding/debugging purposes when vibrator is not available

% we need these to get size of stimuli in visual angles
% measure these two by hand in every new screen
dev.widthOfScreen_inCm      = 34.674; % testing room: 37.478, office: 59.674
dev.heightOfScreen_inCm     = 19.400; % testing room: 29.983, office: 33.566
dev.gamma                   = 2.2; %get gamma value for linear luminance correction

cfg.viewDist = 46;

dev.port_vibrator= 'COM5';

if ~realExperiment
    %PsychDebugWindowConfiguration;
    Screen('Preference', 'SkipSyncTests',1);
    dev.widthOfScreen_inCm      = 37.478;
    dev.heightOfScreen_inCm     = 29.983;
else
    Screen('Preference', 'SkipSyncTests',1);
end

myPath = pwd;
addpath('functions')

dlg = inputdlg({'Name','Age','Sex','Modality (vision=v, tactile=t)','Run (training=t, feedback=f, real=r)','Language (*English*=e, German=g)'}, 'Input');
Info.name           = dlg{1};
Info.age            = str2double(dlg{2});
Info.sex            = dlg{3};
if strcmp(dlg{4},'v')
    cfg.modality.audio  = 0;
    cfg.modality.vision = 1;
    cfg.modality.touch  = 0;
elseif strcmp(dlg{4},'t')
    cfg.modality.audio  = 0;
    cfg.modality.vision = 0;
    cfg.modality.touch  = 1;
else
    error('Please choose a valid modality (v or t)')
end
if strcmp(dlg{5},'t')
    cfg.trainstair = 1;
    cfg.feedback = 0;
elseif strcmp(dlg{5},'f')
    cfg.trainstair = 0;
    cfg.feedback = 1;
elseif strcmp(dlg{5},'r')
    cfg.trainstair = 0;
    cfg.feedback = 0;
else
    error('Please choose a valid run (t, f or r)')
end
cfg.language        = dlg{6};


if isempty(Info.name),Info.name='test';end
if isnan(Info.age),Info.age=99;end
if isempty(Info.sex),Info.sex='?';end
cfg.buzzOrBeep=1;
if isempty(cfg.language),cfg.language='e';end


%% In all conditions, retrieve the balance for both buzz and beep

balanceAudio_buzzFile = ['./data/subj' Info.name '_balanceAudio_buzzOrBeep1'];
balanceAudio_beepFile = ['./data/subj' Info.name '_balanceAudio_buzzOrBeep2'];

if exist([balanceAudio_buzzFile filesep 'alldat_subj' Info.name '_balance.mat'], 'file')
    fromBalance = load([balanceAudio_buzzFile filesep 'alldat_subj' Info.name '_balance.mat']);
    cfg.balanceBuzz = fromBalance.Resu_balanceAudio;
else
    cfg.balanceBuzz = [.5 .5];
end

if exist([balanceAudio_beepFile filesep 'alldat_subj' Info.name '_balance.mat'], 'file')
    fromBalance = load([balanceAudio_beepFile filesep 'alldat_subj' Info.name '_balance.mat']);
    cfg.balanceBeep = fromBalance.Resu_balanceAudio;
else
    cfg.balanceBeep = [.5 .5];
end

%----------------
% Tactile Balance
%----------------
balanceTactile_TopFile = ['./data/subj' Info.name '_balanceTactile_ToporBottom1'];
balanceTactile_BottomFile = ['./data/subj' Info.name '_balanceTactile_ToporBottom2'];

if exist([balanceTactile_TopFile filesep 'alldat_subj' Info.name '_balance.mat'], 'file')
    fromBalance = load([balanceTactileFile_TopFile filesep 'alldat_subj' Info.name '_balance.mat']);
    cfg.balanceTop = fromBalance.Resu_balanceTactile;
else
    cfg.balanceTop = [.5 .5];
end

if exist([balanceTactile_BottomFile filesep 'alldat_subj' Info.name '_balance.mat'], 'file')
    fromBalance = load([balanceTactileFile_BottomFile filesep 'alldat_subj' Info.name '_balance.mat']);
    cfg.balanceBottom = fromBalance.Resu_balanceTactile;
else
    cfg.balanceBottom = [.5 .5];
end

keep cfg dev Info realExperiment myPath

%%

%%% in the bimodal condition, retrieve previous unimodal thresholds
%%% (do this now to avoid messing things up by loading old data)
if ~cfg.trainstair
    
    trial2avg=25; % base the threshold on the last trialtomean trials
    fact2mult= 1; % multiplies the training threshold by fact2mult to produce the actual threshold
    threshaud=0;threshvis=0;threshtouch=0;
    if cfg.modality.vision
        train_vis=['./data/subj' Info.name '_audio0_visual' num2str(cfg.modality.vision) '_tactile0_staircase1_train0'];
        if exist([train_vis filesep 'trialdat_' Info.name '_block1.mat'], 'file')
            fromVision=load([train_vis filesep 'trialdat_' Info.name '_block1.mat']);
            tmpvis=zeros(size(fromVision.Info.T,2),1);
            for t=1:size(tmpvis)
                tmpvis(t)=fromVision.Info.T(t).vision.signalDiff;
            end
            threshvis=min(0.45,mean(tmpvis(length(tmpvis)-trial2avg+1:length(tmpvis)))*fact2mult); % change to .7?
            fprintf('Previous visual threshold is: %.3f \n',threshvis);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while isempty(threshvalidate)
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
            while ~strcmp(threshvalidate,'y')
                threshvis=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshvis);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
 
        else
            threshvis=input('Can''t find previous unimodal visual threshold, input one (e.g. 0.45): ');
            fprintf('You entered as threshold: %.3f\n',threshvis);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while ~strcmp(threshvalidate,'y')
                threshvis=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshvis);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');            
            end
        end
    end
    
    if cfg.modality.audio
        train_aud=['./data/subj' Info.name '_audio' num2str(cfg.modality.audio) '_visual0_tactile0_staircase1_train0'];
        if exist([train_aud filesep 'trialdat_' Info.name '_block1.mat'], 'file')
            fromAudio=load([train_aud filesep 'trialdat_' Info.name '_block1.mat']);
            tmpaud=zeros(size(fromAudio.Info.T,2),1);
            for t=1:size(tmpaud)
                tmpaud(t)=fromAudio.Info.T(t).audio.signalDiff;
            end
            threshaud=min(4,mean(tmpaud(length(tmpaud)-trial2avg+1:length(tmpaud)))*fact2mult);
            fprintf('Previous audio threshold is: %.3f \n',threshaud);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while isempty(threshvalidate)
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
            while ~strcmp(threshvalidate,'y')
                threshaud=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshaud);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
        else
            threshaud=input('Can''t find previous unimodal audio threshold, input one (e.g. 4): ');
            fprintf('You entered as threshold: %.3f\n',threshaud);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while ~strcmp(threshvalidate,'y')
                threshaud=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshaud);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');            
            end
        end
    end
    
    if cfg.modality.touch
        train_touch=['./data/subj' Info.name '_audio0_visual0_tactile' num2str(cfg.modality.touch) '_staircase1_train0'];
        if exist([train_touch filesep 'trialdat_' Info.name '_block1.mat'],'file')
            fromTouch=load([train_touch filesep 'trialdat_' Info.name '_block1.mat']);
            tmptouch=zeros(size(fromTouch.Info.T,2),1);
            for t=1:size(tmptouch)
                tmptouch(t)=fromTouch.Info.T(t).touch.signalDiff;
            end
            threshtouch=round(min(255,mean(tmptouch(length(tmptouch)-trial2avg+1:length(tmptouch)))*fact2mult));
            fprintf('Previous tactile threshold is: %d \n',threshtouch);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while isempty(threshvalidate)
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
            while ~strcmp(threshvalidate,'y')
                threshtouch=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshtouch);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
        else
            threshtouch=input('Can''t find previous unimodal tactile threshold, input one (e.g. 40): ');
            fprintf('You entered as threshold: %.3f\n',threshtouch);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while ~strcmp(threshvalidate,'y')
                threshtouch=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshtouch);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');            
            end
        end
    end
    
    keep Info cfg dev threshvis threshaud threshtouch realExperiment myPath

end

if ((cfg.modality.audio==1 && cfg.modality.vision==0 && cfg.modality.touch==0) || (cfg.modality.audio==0 && cfg.modality.vision==1 && cfg.modality.touch==0) || (cfg.modality.audio==0 && cfg.modality.vision==0 && cfg.modality.touch==1))
    cfg.unimodal_single = 1; %1 for unimodal; 2 for bimodal
else
    cfg.unimodal_single = 0;
end

%this will define which stimulus the staircase is done on.
%If both are true, then the staircase will be selected
%randomly, and there will be one (or two) per modality.
if cfg.modality.audio == 2
    cfg.focus.audio       = 1;
    cfg.focus.vision      = 0;
    cfg.focus.touch       = 0;
elseif cfg.modality.vision == 2
    cfg.focus.touch       = 0;
    cfg.focus.audio       = 0;
    cfg.focus.vision      = 1;
elseif cfg.modality.touch == 2
    cfg.focus.touch       = 1;
    cfg.focus.audio       = 0;
    cfg.focus.vision      = 0;
elseif cfg.modality.vision == 1 && cfg.modality.audio == 0 && cfg.modality.touch == 0
    cfg.focus.audio       = 0;
    cfg.focus.vision      = 1;
    cfg.focus.touch       = 0;
elseif cfg.modality.vision == 0 && cfg.modality.audio == 1 && cfg.modality.touch == 0
    cfg.focus.audio       = 1;
    cfg.focus.vision      = 0;
    cfg.focus.touch       = 0;
elseif cfg.modality.vision == 0 && cfg.modality.audio == 0 && cfg.modality.touch == 1
    cfg.focus.audio       = 0;
    cfg.focus.vision      = 0;
    cfg.focus.touch       = 1;
end

commandwindow;                                                              % go to commandwindow to prevent writing on the script
% -----------------------
% Parameters Modifiables:
% -----------------------

saveDir         = [myPath filesep 'data' filesep 'subj' Info.name '_audio' num2str(cfg.modality.audio) '_visual' num2str(cfg.modality.vision) '_tactile' num2str(cfg.modality.touch) '_staircase' num2str(cfg.trainstair) '_train' num2str(cfg.feedback)];

if ~isdir(saveDir), mkdir(saveDir),end

cfg.stim.vision.version = 'gabor';

cfg.audio.percentDiff       = [.1 4];                                           % minimal / maximal percent difference tested
if cfg.trainstair && cfg.modality.audio == 1
    cfg.audio.initialDiff(1)  = 2;
    cfg.audio.initialDiff(2)  = .1;
else
    cfg.audio.initialDiff(1)  = 4;
    cfg.audio.initialDiff(2)  = .1;
end
cfg.audio.stepsize          = 0.1;
cfg.audio.levels            = (cfg.audio.percentDiff(1) : cfg.audio.stepsize : cfg.audio.percentDiff(2));

if strcmp(cfg.stim.vision.version,'dots')
    cfg.vision.percentDiff      = [.02 4];
    cfg.vision.initialDiff(1)   = .4;
    cfg.vision.initialDiff(2)   = .02;
    cfg.vision.stepsize         = .02;                                              % reference is 50 dots, so 1 dot is 0.02
elseif strcmp(cfg.stim.vision.version,'gabor')
    cfg.stim.vision.baseContrast= .5;
    cfg.vision.percentDiff      = [.05 1-cfg.stim.vision.baseContrast];             % this is actually not percent but absolute contrast diff
    cfg.vision.initialDiff(1)   = .4; % this must not be higher than 1-cfg.stim.vision.baseContrast !
    cfg.vision.initialDiff(2)   = .05;
    cfg.vision.stepsize         = .025;
end
cfg.vision.levels           = (cfg.vision.percentDiff(1) : cfg.vision.stepsize : cfg.vision.percentDiff(2));

cfg.stim.touch.baseVibration = 90;
cfg.touch.initialDiff(1) = 40;
cfg.touch.initialDiff(2) = 5;
cfg.touch.stepsize       = 2;
cfg.touch.percentDiff    = [1 255-1]; % maximum signal strength is 255
cfg.touch.levels         = (cfg.touch.percentDiff(1) : cfg.touch.stepsize : cfg.touch.percentDiff(2));

cfg.stim.duration           = 0.2;                                             % duration of stimuli (seconds)
cfg.stim.audio.freqs        = [200 1100];                                       % tone frequencies [440 2050]

cfg.stim.audio.rampDur      = .001;                                             % duration of the ramp gradually turning the sound on and off
cfg.stim.audio.sampRate     = 44100;                                            % Sampling Rate Hz
cfg.stim.audio.nTimeSamples = cfg.stim.duration*cfg.stim.audio.sampRate;        % number of time samples TOJ sound

cfg.directions              = [1 2];                                            % possible directions, number of conditions for SOA @ threshold
cfg.congruency              = [0 1];                                            % incongruent or congruent trials

%cfg.trialsPerCond*2 = real length of experiment
switch cfg.trainstair
    case 0
        cfg.trialsPerCond         = 120; %keep this multiple of 4             % N trials in each strongDirection condition (L-R / R-L)
        cfg.NBlocks               = 4;                                        % Expe divided in NBlocks blocks
    case 1
        cfg.trialsPerCond         = 40;  %keep this multiple of 4             % N trials in each strongDirection condition (L-R / R-L)
        cfg.NBlocks               = 1;                                        % Expe divided in NBlocks blocks
end

cfg.trialsPerBlock        = 2*cfg.trialsPerCond/cfg.NBlocks;
cfg.nStairs               = 1;                                                %2 or 1

% ----------------------
% set general visual parameters
% ----------------------
bg           = .5 .^ (1/dev.gamma);
dev.grey     = round(255*[bg bg bg]);

% -----------
% PTB screen:
% -----------
dev.screenNumber        = min(Screen('Screens'));

if realExperiment
    [dev.wnd, dev.screenRect]   = Screen('OpenWindow', dev.screenNumber, dev.grey, [], 32,2); %complete screen
else
    [dev.wnd, dev.screenRect]   = Screen('OpenWindow', dev.screenNumber, dev.grey, [0 0 1280 1024], 32,2); %small screen
end

%load MyGammaTable
%Screen('LoadNormalizedGammaTable', dev.wnd, gammaTable*[1 1 1])


dev.refreshrate_in_Hz         = Screen('FrameRate', dev.screenNumber);
dev.refreshtime_in_ms         = 1000/dev.refreshrate_in_Hz;
Screen('TextSize',dev.wnd,36);

dev.yCenter         = (dev.screenRect(4) - dev.screenRect(2))/2 + dev.screenRect(2);
dev.xCenter         = (dev.screenRect(3) - dev.screenRect(1))/2 + dev.screenRect(1);


if realExperiment
    HideCursor; % Hide the mouse cursor
end

% -------------
% VAS settings:
% -------------

VAS_Visang              = 20;                                              
ScreenWidthDegrees      = visang(cfg.viewDist, [], dev.widthOfScreen_inCm);
pixPerDeg               = (dev.screenRect(3) - dev.screenRect(1))/ScreenWidthDegrees;
VAS_size                = VAS_Visang * pixPerDeg; 

cfg.txtCol              = 255 * [0 0 0];
cfg.cursorCol           = 255 * [0 0 0];
cfg.maxConfRT           = Inf;            %secs
if cfg.language=='e'
    stringUnsure           = 'Very unsure';
    stringSure             = 'Very sure';
    cfg.VAS.questionString = 'How confident are you?';
elseif cfg.language=='g'
    stringUnsure           = 'Sehr unsicher';
    stringSure             = 'Sehr sicher';
    cfg.VAS.questionString = 'Wie sicher sind Sie?';
end
cfg.VAS.nBands          = 10;
if cfg.unimodal_single
    cfg.VAS.bandWidth   = 20;
    cfg.VAS.bandLength  = round(dev.yCenter/10);
    cfg.VAS.stringLow   = stringUnsure;
    cfg.VAS.stringHigh  = stringSure;
else
    cfg.VAS.bandHeight  = 20;
    cfg.VAS.bandWidth   = VAS_size/cfg.VAS.nBands;%round(dev.xCenter/14);     %width of each colour band in the confidence scale
    cfg.VAS.stringLeft  = stringUnsure;
    cfg.VAS.stringRight = stringSure;
end
cfg.VAS.colorLight      = round(1/2 * 255 * [1 1 1]);
cfg.VAS.colorDark       = 255 * [1 1 1];

    
% -----
% Keys:
% -----

KbName('UnifyKeyNames');
cfg.quitkey = KbName('ESCAPE');

cfg.leftAnswerKey     = KbName('LeftArrow');
cfg.rightAnswerKey    = KbName('RightArrow');
cfg.congruentKey      = KbName('W');
cfg.incongruentKey    = KbName('S');
cfg.Question          = '';

if cfg.unimodal_single==1
   cfg.signResponseRight = '<--';
   cfg.signResponseLeft  = '-->';
   if cfg.language=='e'
       cfg.legendRight       = 'Left';
       cfg.legendLeft        = 'Right';
   elseif cfg.language=='g'
       cfg.legendRight       = 'Links';
       cfg.legendLeft        = 'Rechts';
   end
else
  cfg.signResponseRight = 'W';
  cfg.signResponseLeft  = 'S';
  if cfg.language=='e'
      cfg.legendRight       = 'Same';
      cfg.legendLeft        = 'Different';
  elseif cfg.language=='g'
      cfg.legendRight       = 'Gleich';
      cfg.legendLeft        = 'Unterschiedlich';
  end
end

%-------
% Mouse:
%-------

%This we'll have to change a bit
mouseId = GetMouseIndices;
mouseId = mouseId(1);

dev.mouseId = mouseId;

%----------------------------
% Visual stimulus parameters
%----------------------------

if strcmp(cfg.stim.vision.version,'gabor')
    setGaborParams();
elseif strcmp(cfg.stim.vision.version,'dots')
    setDotParams();
end

%-----------------------------------------------------------
% Generate Sounds for judgement, one sin wave, one sawtooth:
%-----------------------------------------------------------

Sc =  sawtooth(2*pi*cfg.stim.audio.freqs(1)*(0:cfg.stim.duration*cfg.stim.audio.sampRate)/cfg.stim.audio.sampRate);

%Generate baseline sound waves to be modulated in each trial according to a staircase
%pitch 1
Sound_Left{1}  = [cfg.balanceBuzz(1)*Sc;zeros(1,length(Sc))];
Sound_Right{1} = [zeros(1, length(Sc));cfg.balanceBuzz(2)*Sc];

Sc = MakeBeep(cfg.stim.audio.freqs(2),cfg.stim.duration,[cfg.stim.audio.sampRate]);

%Generate baseline sound waves to be modulated in each trial according to a staircase
%pitch 2
Sound_Left{2}  = [cfg.balanceBeep(1)*Sc;zeros(1,length(Sc))];
Sound_Right{2} = [zeros(1, length(Sc));cfg.balanceBeep(2)*Sc];


% ----------------------------------------
% Randomization of the trial presentation:
% ----------------------------------------
% Define trials


% -----------
% Staircase:
% -----------

% All staircases initialized in all cases, not always used

% for test condition, use previous training thresholds
%P.audio.initialDiff=2;P.vision.initialDiff=0.3;
switch cfg.trainstair
    case 0
        for i=1:cfg.nStairs       % this floor()... calculation makes sure to start with a difference of dots that's a multiple of the step size
            Info.staircaseAudio(i)  = SetupStaircase(1, floor(threshaud/cfg.audio.stepsize)*cfg.audio.stepsize, [min(cfg.audio.percentDiff) max(cfg.audio.percentDiff)], [2 1]);
            Info.staircaseVision(i) = SetupStaircase(1, floor(threshvis/cfg.vision.stepsize)*cfg.vision.stepsize, [min(cfg.vision.percentDiff) max(cfg.vision.percentDiff)], [2 1]);
            Info.staircaseTouch(i)  = SetupStaircase(1, floor(threshtouch/cfg.touch.stepsize)*cfg.touch.stepsize, [min(cfg.touch.percentDiff) max(cfg.touch.percentDiff)], [2 1]);
        end
    case 1
        for i=1:cfg.nStairs
            Info.staircaseAudio(i)  = SetupStaircase(1, cfg.audio.initialDiff(i), [min(cfg.audio.percentDiff) max(cfg.audio.percentDiff)], [2 1]);
            Info.staircaseVision(i) = SetupStaircase(1, cfg.vision.initialDiff(i), [min(cfg.vision.percentDiff) max(cfg.vision.percentDiff)], [2 1]);
            Info.staircaseTouch(i)  = SetupStaircase(1, cfg.touch.initialDiff(i), [min(cfg.touch.percentDiff) max(cfg.touch.percentDiff)], [2 1]);
        end
end

%-------------------------------------------------------------
% PSEUDORANDOMIZED ORDER OF STIMS & STAIRCASES:
%-------------------------------------------------------------

[congruency]=randConstrNathan(cfg.trialsPerCond, cfg.congruency, 5);

%%% Make sure we have a balanced design between congruency and strongDirection
[strongDirection1]=randConstrNathan(cfg.trialsPerCond/2,  cfg.directions, 5);
[strongDirection2]=randConstrNathan(cfg.trialsPerCond/2,  cfg.directions, 5);

strongDirection(congruency==0)=strongDirection1;
strongDirection(congruency==1)=strongDirection2;

% ------------------------------------------
% Set up staircase for the modality in focus
% ------------------------------------------
StairChoice = repmat([1 cfg.nStairs ],1,length(strongDirection)/2);          % order of ascending/descending staircases
StairChoice = StairChoice(randperm(length(StairChoice)));

for itrial=1:length(strongDirection)
        
    Info.T(itrial).stair                    = StairChoice(itrial);

    Info.T(itrial).congruency               = congruency(itrial);
    Info.T(itrial).audio.strongDirection    = strongDirection(itrial);     %strongDirection(1) is stim 1
    if ~(cfg.modality.audio==1 && cfg.modality.touch==1)
        Info.T(itrial).touch.strongDirection    = strongDirection(itrial);
    end
    
    %set the strongDirection specifically for each of the two stimuli
    if congruency(itrial)
        Info.T(itrial).vision.strongDirection  = strongDirection(itrial);
        if cfg.modality.audio==1 && cfg.modality.touch==1
            Info.T(itrial).touch.strongDirection  = strongDirection(itrial);
        end
    else
        Info.T(itrial).vision.strongDirection  = 3 - strongDirection(itrial);   %if 1--> 2, if 2--> 1
        if cfg.modality.audio==1 && cfg.modality.touch==1
            Info.T(itrial).touch.strongDirection  = 3 - strongDirection(itrial);
        end
    end
end

if cfg.modality.touch>0 && ~cfg.novib
    %-----------------------------------
    % Open the serial port for vibrator:
    %-----------------------------------
    vibrator = serial(dev.port_vibrator,'Baud',115200);
    vibrator.InputBufferSize=256;
    fopen(vibrator);
else
    vibrator = 0;
end

if cfg.modality.audio>0
    % -------------------------------------------------
    % Perform basic initialization of the sound driver:
    % -------------------------------------------------

    InitializePsychSound(1);
    %dev.pamaster = PsychPortAudio('Open', 3, 1+8, 1,44100, 2, [],1);
    %dev.pamaster = PsychPortAudio('Open', [], 1+8, 2, 44100, 2);
    dev.pamaster = PsychPortAudio('Open', [], 1+8, 1,44100, 2, [],1);  % if the sound is not played on the headphones, try the other line instead

    %----------------------
    % Prepare Audio device:
    %----------------------
    PsychPortAudio('Start',dev.pamaster, 0,0,1);
    dev.paslave  = PsychPortAudio('OpenSlave', dev.pamaster, 1,2);
end

%-------------------------------------------------------------
%                  Start  Experiment:
%-------------------------------------------------------------

cfg.experiment_aborted = 0;

if cfg.language=='e'
    DrawFormattedText(dev.wnd,'Welcome! \n\n\n Press a key to start.','center','center');
elseif cfg.language=='g'
    DrawFormattedText(dev.wnd,'Wilkommen! \n\n\n Druecken Sie eine Taste, um anzufangen.','center','center');
end
Screen('Flip',dev.wnd);

keyIsDown=0;
while keyIsDown==0
    [keyIsDown,~,~] = KbCheck;
end
startExperimentTime = tic;

for  t=1:length(Info.T)
    
    %---------------------------------------------------
    % Presentation of fixation cross for the ITI length:
    %---------------------------------------------------
    Screen('TextSize',dev.wnd,36)
    DrawFormattedText(dev.wnd,'+','center','center');
    Screen('TextSize', dev.wnd, 36)
    DrawFormattedText(dev.wnd, [num2str(t) '   '], 'right')
    Screen('Flip',dev.wnd);
    
    Info.T(t).ITI = (0.5-rand(1))+1;  % intertrial interval in seconds
    WaitSecs(Info.T(t).ITI);
    
    %----------------------------
    % Get the signal intensity difference from staircase:
    %----------------------------
    
    stair = Info.T(t).stair; % choose up/down staircase
    
    %Get the stimulus contrast for the relevant stimulus / stimuli from the
    %respective staircase(s)
    if cfg.modality.vision && cfg.modality.audio                            % bimodal
        Info.T(t).vision.signalDiff      = Info.staircaseVision(stair).Signal;
        Info.T(t).audio.signalDiff       = Info.staircaseAudio(stair).Signal;
        Info.T(t).touch.signalDiff       = 0;
    elseif cfg.modality.vision && cfg.modality.touch
        Info.T(t).vision.signalDiff      = Info.staircaseVision(stair).Signal;
        Info.T(t).audio.signalDiff       = 0;
        Info.T(t).touch.signalDiff       = Info.staircaseTouch(stair).Signal; 
    elseif cfg.modality.audio && cfg.modality.touch
        Info.T(t).vision.signalDiff      = 0;
        Info.T(t).audio.signalDiff       = Info.staircaseAudio(stair).Signal;
        Info.T(t).touch.signalDiff       = Info.staircaseTouch(stair).Signal;         
    elseif cfg.focus.vision                                                 % unimodal vision
        Info.T(t).vision.signalDiff      = Info.staircaseVision(stair).Signal;
        Info.T(t).audio.signalDiff       = 0;
        Info.T(t).touch.signalDiff       = 0;
    elseif cfg.focus.audio                                                  % unimodal audio
        Info.T(t).vision.signalDiff      = 0;
        Info.T(t).audio.signalDiff       = Info.staircaseAudio(stair).Signal;
        Info.T(t).touch.signalDiff       = 0;
    else                                                                    % unimodal touch
        Info.T(t).vision.signalDiff      = 0;
        Info.T(t).audio.signalDiff       = 0;
        Info.T(t).touch.signalDiff       = Info.staircaseTouch(stair).Signal;  
    end
    
    
    %--------------------------------------
    % Set stimuli with appropriate signal strength:
    %--------------------------------------
    
    if Info.T(t).audio.strongDirection == 1
        toggle(1:2) = [1 0];
    elseif Info.T(t).audio.strongDirection == 2
        toggle(1:2) = [0 1];
    end
    if Info.T(t).congruency
        toggle(3:4) = toggle(1:2);
    else
        toggle(3:4) = 1 - toggle(1:2);
    end
    
    if cfg.modality.audio == 2
        soundLeft_freq1  = Sound_Left{1}  + toggle(1) * Info.T(t).audio.signalDiff * Sound_Left{1}; %strongDirection == 1 means left
        soundRight_freq1 = Sound_Right{1} + toggle(2) * Info.T(t).audio.signalDiff * Sound_Right{1};
        soundLeft_freq2  = Sound_Left{2}  + toggle(3) * Info.T(t).audio.signalDiff * Sound_Left{2};
        soundRight_freq2 = Sound_Right{2} + toggle(4) * Info.T(t).audio.signalDiff * Sound_Right{2};
    elseif cfg.modality.audio == 1
        soundLeft_freq1  = Sound_Left{1}  + toggle(1) * Info.T(t).audio.signalDiff * Sound_Left{1}; %strongDirection == 1 means left
        soundRight_freq1 = Sound_Right{1} + toggle(2) * Info.T(t).audio.signalDiff * Sound_Right{1};
        soundLeft_freq2  = Sound_Left{2}  + toggle(1) * Info.T(t).audio.signalDiff * Sound_Left{2};
        soundRight_freq2 = Sound_Right{2} + toggle(2) * Info.T(t).audio.signalDiff * Sound_Right{2};
    end
    
    if cfg.modality.audio == 2
        totalSound_Left  = (soundLeft_freq1  + soundLeft_freq2)/2;
        totalSound_Right = (soundRight_freq1 + soundRight_freq2)/2;
        totalSound= (totalSound_Left+totalSound_Right)/2;
        
    elseif cfg.modality.audio == 1
        if cfg.buzzOrBeep == 1
            totalSound_Left  = soundLeft_freq1;
            totalSound_Right = soundRight_freq1;
            totalSound= (totalSound_Left+totalSound_Right)/2;
        else
            totalSound_Left  = soundLeft_freq2;
            totalSound_Right = soundRight_freq2;
            totalSound= (totalSound_Left+totalSound_Right)/2;
        end
    end
    
    
    %Visual
    if strcmp(cfg.stim.vision.version,'gabor')
        
        %make gabor ad hoc the quests suggestion
        this_im_dim = cfg.stim.vision.base_Grate * (cfg.stim.vision.baseContrast + Info.T(t).vision.signalDiff);     % modulate contrast
        this_imP    = (this_im_dim'+1)/2;                                   % convert -1->1 to 0->1
        this_imG    = round(255 .* (this_imP .^  (1 / dev.gamma)));
        above_GaborTex = Screen('MakeTexture', dev.wnd, this_imG);        
        
        if Info.T(t).congruency
            secondGaborPair = [3 4];
        else
            secondGaborPair = [4 3];
        end
        
    elseif strcmp(cfg.stim.vision.version,'dots')
        
        %n = cfg.stim.vision.REF*(1+(2*(rand>.5)-1).*max(0,Info.T(t).vision.signalDiff)); % either greater or less than standard, random  (staircase can go negative but only values >=0 are used, avoids boundary problem)
        n = cfg.stim.vision.REF*(1+Info.T(t).vision.signalDiff);
        n = round(n);
        n = max(1,n);
        n = min(n,2*cfg.stim.vision.REF);    % bound by 1 and 2*REF

        n_high = max([n, cfg.stim.vision.REF]);
        n_low  = min([n, cfg.stim.vision.REF]);
    
        if Info.T(t).vision.strongDirection == 1
            toggle(1:2) = [1 0];
        elseif Info.T(t).vision.strongDirection == 2
            toggle(1:2) = [0 1];
        end
        if Info.T(t).congruency
            toggle(3:4) = toggle(1:2);
        else
            toggle(3:4) = 1 - toggle(1:2);
        end

        if cfg.modality.vision == 2
            n = toggle * n_high + (1-toggle) * n_low;
        elseif cfg.modality.vision == 1
            n = toggle(1:2) * n_high + (1-toggle(1:2)) * n_low;
        end
    end
    
    %touch
    
    if Info.T(t).touch.strongDirection == 1
        toggle(1:2) = [1 0];
    elseif Info.T(t).touch.strongDirection == 2
        toggle(1:2) = [0 1];
    end
    if Info.T(t).congruency
        toggle(3:4) = toggle(1:2);
    else
        toggle(3:4) = 1 - toggle(1:2);
    end
    if cfg.modality.touch == 2
        vibLeft1  = 2*cfg.balanceTop(1)*cfg.stim.touch.baseVibration  + toggle(1) * Info.T(t).touch.signalDiff; %strongDirection == 1 means left
        vibRight1  = 2*cfg.balanceTop(2)*cfg.stim.touch.baseVibration + toggle(2) * Info.T(t).touch.signalDiff;
        vibLeft2  =  2*cfg.balanceBottom(1)*cfg.stim.touch.baseVibration  + toggle(3) * Info.T(t).touch.signalDiff; %strongDirection == 1 means left
        vibRight2  =  2*cfg.balanceBottom(2)*cfg.stim.touch.baseVibration + toggle(4) * Info.T(t).touch.signalDiff;
    elseif cfg.modality.touch == 1
        vibLeft1  =  2*cfg.balanceTop(1)*cfg.stim.touch.baseVibration  + toggle(1) * Info.T(t).touch.signalDiff; %strongDirection == 1 means left
        vibRight1  =  2*cfg.balanceTop(2)*cfg.stim.touch.baseVibration + toggle(2) * Info.T(t).touch.signalDiff;
        vibLeft2  = 0;
        vibRight2  = 0;
    end
    
    %-------------------
    % Display the stimuli
    %-------------------
    
    whentoplay=GetSecs+0.1;
    
    if cfg.modality.vision
        
        if strcmp(cfg.stim.vision.version,'gabor')
            
            Screen('DrawTextures', dev.wnd, cfg.stim.vision.base_GaborTex, [], cfg.stim.vision.gratingRect, 0);
            Screen('DrawTextures', dev.wnd, above_GaborTex, [], cfg.stim.vision.gratingRect(:,Info.T(t).vision.strongDirection), 0);
            if cfg.modality.vision == 2
                Screen('DrawTextures', dev.wnd, above_GaborTex, [], cfg.stim.vision.gratingRect(:,secondGaborPair(Info.T(t).vision.strongDirection)), 0);
            end
            
        elseif strcmp(cfg.stim.vision.version,'dots')
            
            Screen('FrameOval',dev.wnd,cfg.stim.vision.dotcolor,cfg.stim.vision.gratingRect(:,1),cfg.stim.vision.pen_width);
            Screen('FrameOval',dev.wnd,cfg.stim.vision.dotcolor,cfg.stim.vision.gratingRect(:,2),cfg.stim.vision.pen_width);
            if cfg.modality.vision==2
                Screen('FrameOval',dev.wnd,cfg.stim.vision.dotcolor,cfg.stim.vision.gratingRect(:,3),cfg.stim.vision.pen_width);
                Screen('FrameOval',dev.wnd,cfg.stim.vision.dotcolor,cfg.stim.vision.gratingRect(:,4),cfg.stim.vision.pen_width);    
            end

            for side = 1:size(n,2)
                xy = dotcloud(n(side),cfg.stim.vision.dotsize);
                xy = xy*0.95*cfg.stim.vision.inner_circle*dev.fov;
                xy = xy/2*eye(2);
                Info.T(t).vision.drawnXY{side} = xy;

                z = cfg.stim.vision.dotsize*0.95*cfg.stim.vision.inner_circle*dev.fov;

                for i=1:n(side)
                    wh = xy(i,[1 2 1 2])  +  [-z/2 -z/2 z/2 +z/2] + [ cfg.stim.vision.centers(:,side); cfg.stim.vision.centers(:,side) ]';
                    Screen('FillOval', dev.wnd, cfg.stim.vision.dotcolor,wh);
                end
            end
        end
        Screen('TextSize', dev.wnd, 36);
        DrawFormattedText(dev.wnd, [num2str(t) '   '], 'right')
        Info.T(t).vision.onset = Screen('Flip', dev.wnd, whentoplay);
    end
    
    %-------------------
    % And/or Play the Sounds:
    %-------------------
    
    if cfg.modality.audio
        PsychPortAudio('FillBuffer', dev.paslave, totalSound);
        Info.T(t).audio.onset = PsychPortAudio('Start', dev.paslave, 1, whentoplay,1);
    end
    
    %---------------------------
    %  And/Or Start the Vibration
    %---------------------------
    if cfg.modality.touch
        Screen('TextSize', dev.wnd, 36);
        DrawFormattedText(dev.wnd, [num2str(t) '   '], 'right')
        Info.T(t).touch.onset = Screen('Flip', dev.wnd, whentoplay);
        if ~cfg.novib
            % send serial order (to pins 6, 9, 10, 11)
            fprintf(vibrator,sprintf('s%03da%03da%03da%03d',vibLeft1,vibRight1,vibLeft2,vibRight2));
        end
    end
    
    
    WaitSecs(cfg.stim.duration);
    if cfg.modality.audio
        PsychPortAudio('Stop', dev.paslave,1);
    end
    if cfg.modality.touch && ~cfg.novib
        fprintf(vibrator,sprintf('s%03da%03da%03da%03d',0,0,0,0));
    end
    
    
    %--------------------------------
    %Start checking for the response:
    %--------------------------------
    if cfg.unimodal_single==1
        RestrictKeysForKbCheck([cfg.leftAnswerKey cfg.rightAnswerKey cfg.quitkey]);
    else
        RestrictKeysForKbCheck([cfg.congruentKey cfg.incongruentKey cfg.quitkey]);
    end
    Screen('TextSize', dev.wnd, 36);
    DrawFormattedText(dev.wnd, [num2str(t) '   '], 'right')
    Screen('TextSize', dev.wnd, 36);
    DrawFormattedText(dev.wnd, cfg.Question,'center','center',0);
    Screen('TextSize', dev.wnd, 40); DrawFormattedText(dev.wnd, cfg.signResponseRight, 'center' , dev.yCenter- 50,0);
    Screen('TextSize', dev.wnd, 36); DrawFormattedText(dev.wnd, cfg.legendRight, 'center' , dev.yCenter - 80,0);
    Screen('TextSize', dev.wnd, 40); DrawFormattedText(dev.wnd, cfg.signResponseLeft, 'center' , dev.yCenter+ 80,0);
    Screen('TextSize', dev.wnd, 36); DrawFormattedText(dev.wnd, cfg.legendLeft, 'center' , dev.yCenter+50,0);
    Screen('Flip',dev.wnd);
    
    keyIsDown = 0;
    
    while keyIsDown == 0 %if the key is not down, go into the loop
        [keyIsDown, ~, keyCode] = KbCheck; %read the keyboard
    end
    if keyIsDown && keyCode(cfg.quitkey)
        cfg.experiment_aborted = 1;
        break
    end
    
    switch cfg.modality.vision
        case 0
            if cfg.focus.audio
                Info.T(t).RT = GetSecs-Info.T(t).audio.onset; % ref= audio onset
            else
                Info.T(t).RT = GetSecs-Info.T(t).touch.onset; % ref= touch onset
            end
        otherwise
            Info.T(t).RT = GetSecs-Info.T(t).vision.onset; % ref= visual onset
    end
    
    
    %-----------------------------------------------------
    % Calculate if response was correct or not & feedback:
    %-----------------------------------------------------
    
    if KbCheck == 1
        wasCong = Info.T(t).congruency;     
        
        % in case we do unimodal experiment with only one pair of stimuli
        if cfg.modality.audio == 1 && cfg.modality.vision==0 && cfg.modality.touch==0
            if  keyCode(cfg.leftAnswerKey) && Info.T(t).audio.strongDirection==1
                Info.T(t).ResponseCorrect = 1;
            elseif keyCode(cfg.rightAnswerKey) && Info.T(t).audio.strongDirection==2
                Info.T(t).ResponseCorrect = 1;
            else
                Info.T(t).ResponseCorrect = 0;
            end
        elseif cfg.modality.vision == 1 && cfg.modality.audio==0 && cfg.modality.touch==0
            if  keyCode(cfg.leftAnswerKey) && Info.T(t).vision.strongDirection==1
                Info.T(t).ResponseCorrect = 1;
            elseif keyCode(cfg.rightAnswerKey) && Info.T(t).vision.strongDirection==2
                Info.T(t).ResponseCorrect = 1;
            else
                Info.T(t).ResponseCorrect = 0;
            end
        elseif cfg.modality.touch == 1 && cfg.modality.audio==0 && cfg.modality.vision==0
            if keyCode(cfg.leftAnswerKey) && Info.T(t).touch.strongDirection==1
                Info.T(t).ResponseCorrect = 1;
            elseif keyCode(cfg.rightAnswerKey) && Info.T(t).touch.strongDirection==2
                Info.T(t).ResponseCorrect = 1;
            else
                Info.T(t).ResponseCorrect = 0;
            end
            
        % for unimodal staircases before unimodal conditions, and main experiment
        else
            if  keyCode(cfg.congruentKey) && wasCong
                Info.T(t).ResponseCorrect = 1;
                
            elseif keyCode(cfg.congruentKey) && ~wasCong
                Info.T(t).ResponseCorrect = 0;
                
            elseif keyCode(cfg.incongruentKey) && ~wasCong
                Info.T(t).ResponseCorrect = 1;
                
            elseif keyCode(cfg.incongruentKey) && wasCong
                Info.T(t).ResponseCorrect = 0;
                
            end
        end
        
    end
    
    % sprintf('Congruency: %d',Info.T(t).congruency)
    % store response in staircase
    Info.staircaseAudio(stair) = StaircaseTrial(1, Info.staircaseAudio(stair), Info.T(t).ResponseCorrect);
    Info.staircaseAudio(stair) = UpdateStaircase(1, Info.staircaseAudio(stair), -cfg.audio.stepsize);         %this -stepsize is confusing, but correct
    
    Info.staircaseVision(stair) = StaircaseTrial(1, Info.staircaseVision(stair), Info.T(t).ResponseCorrect);
    Info.staircaseVision(stair) = UpdateStaircase(1, Info.staircaseVision(stair), -cfg.vision.stepsize);
    
    Info.staircaseTouch(stair) = StaircaseTrial(1, Info.staircaseTouch(stair), Info.T(t).ResponseCorrect);
    Info.staircaseTouch(stair) = UpdateStaircase(1, Info.staircaseTouch(stair), -cfg.touch.stepsize);
    
    %-----------------------------
    % Ask for confidence judgment in test condition only
    %-----------------------------
    if ~cfg.trainstair
        %show VAS, wait for answer, etc
        if cfg.unimodal_single
            [data] = collectVASjudge_vertical(t, cfg.feedback, Info.T(t).ResponseCorrect);
        else
            [data] = collectVASjudge(cfg.feedback, Info.T(t).ResponseCorrect);
        end
        Info.T(t).VAS = data.VAS;
        commandwindow;              % go to commandwindow to prevent writing on the script
        if cfg.experiment_aborted
            break
        end
    end
    
    % Close Textures
    if strcmp(cfg.stim.vision.version,'gabor')
        Screen('Close', above_GaborTex)
    end
    
    if ~mod(t,cfg.trialsPerBlock)
        Screen('TextSize',dev.wnd,30);
        if t ~= length(Info.T)
            if cfg.language=='e'
                DrawFormattedText(dev.wnd,'You can now take a break. \n\n\n Press C when you are ready to start.','center','center');
            elseif cfg.language=='g'
                DrawFormattedText(dev.wnd,'Sie koennen jetzt eine Pause machen. \n\n\n Druecken Sie C, wenn Sie fortfahren moechten.','center','center');
            end
        else
            if cfg.language=='e'
                DrawFormattedText(dev.wnd,'That was the last one! Press C to end.','center','center');
            elseif cfg.language=='g'
                DrawFormattedText(dev.wnd,'Das war der Letzte! Druecken Sie C, um das Experiment zu beenden.','center','center');
            end
        end
        Screen('Flip',dev.wnd);
        %%%% save relevant infos per block in case of a crash
        save([saveDir filesep 'trialdat_' Info.name '_block' num2str(ceil(t/cfg.trialsPerBlock)) '.mat']);
        
        RestrictKeysForKbCheck(KbName('C'));
        KbWait;
    end   
    
end

WaitSecs(.2);
Screen('CloseAll')
experimentDuration = toc(startExperimentTime);
disp(['This took ' num2str(experimentDuration) ' seconds.'])

keep t dev cfg realExperiment myPath saveDir Info startExperimentTime experimentDuration vibrator

%    save([saveDir filesep 'TOJ_Audio_' name '_' datestr(now,'dd.mm.yyyy') '.mat'], 'Info', 'P');
%%% save everything at the end in case we forgot something
save([saveDir filesep 'alldat_' Info.name '_' datestr(now,'dd.mm.yyyy') '.mat']);

fprintf('\n-----------------------\n\n');

if cfg.modality.audio>0
    PsychPortAudio('Close' , dev.pamaster);
end

if cfg.modality.touch>0
    fclose(vibrator);
    delete(vibrator)
    clear vibrator
end


% workaround bug plot if script stops before the end
if t~=length(Info.T),t=t-1;end

% plot the staircases
for tr = 1:t
    stair(tr) = Info.T(tr).stair;
end


figure; hold on;
xlabel('trial number'); ylabel('signalDiff (sec)'); title('Check staircases')

if cfg.focus.vision == 1
    for tr = 1:t
        vision.signalDiffs(tr)= Info.T(tr).vision.signalDiff;
    end
    plot(vision.signalDiffs(stair==1), 'bo-');
    plot(vision.signalDiffs(stair==2), 'ro-');
end

if cfg.focus.audio == 1
    for tr = 1:t
        audio.signalDiffs(tr)= Info.T(tr).audio.signalDiff;
    end
    plot(audio.signalDiffs(stair==1), 'b*-');
    plot(audio.signalDiffs(stair==2), 'r*-');
end

if cfg.focus.touch ==1
    for tr = 1:t
        touch.signalDiffs(tr) = Info.T(tr).touch.signalDiff;
    end
    plot(touch.signalDiffs(stair==1), 'bx-');
    plot(touch.signalDiffs(stair==2), 'rx-');
end
savefig([saveDir filesep 'staircase.fig'])


% plot raw confidence histogram
if ~ cfg.trainstair
    conf = zeros(t,1);
    for tr = 1:t
        conf(tr)= Info.T(tr).VAS.confidence;
    end
    figure;
    histogram(conf, 0:0.1:1)
    savefig([saveDir filesep 'districonf.fig'])
end

end