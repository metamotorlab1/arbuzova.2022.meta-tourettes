% Polina Arbuzova, May 2020
%% Export staircases data in TS study and variability of difficulty and plot on separate plots


subjectNames = {'h1','h2','h3','h4','h5','h7','h8','h9','h10','h11','h12','h13','h14','h15','h16','h17','h18','h19', 'h20', 'h21', 'h22', 'h23', 'h24', 'h25'};;   % Put all subjects to analyse in this cell array
%subjectNames = {'t1','t2','t3','t4','t5','t6','t7','t8','t9','t10','t11','t12','t13','t14','t15','t16','t17','TS18','TS20', 't21', 't22', 't23', 't24', 't25', 't26', 't27'};;   % Put all subjects to analyse in this cell array
%subjectNames = {'t1','t2'};   % Put all subjects to analyse in this cell array

user = 'polina';

for i=1:numel(subjectNames)
    [staircases_All(i)] = singleSubjectAnalysis(subjectNames{i}, user);
end

resFolder = '/Users/polinaarbuzova/Documents/TS study/Results';
cd(resFolder);
save('hc_staircases_fullDataset.mat')
%% singleSubjectAnalysis
% This runs a bunch of analyses on a subject's data and creates 2 figures:
% one for vision and one for touch.
%
% subj: name of the subject
% user: For each user, you need to add a case and define the dataPath
function [diff] = singleSubjectAnalysis(subj, user)

% data path and functions path
switch user
    case {'christina_home'}
        dataPath = 'C:\Users\christina\Dropbox\metacognition experiment\data';
    case {'polina'}
        dataPath = '/Users/polinaarbuzova/Documents/TS study/Data';
end
functionPath = 'fx';
addpath(functionPath);

% load data
[params, data] = loadData(dataPath, subj);

% set labels and ranges
labels.vision = 'contrast diff.';
labels.touch  = 'vibration strength diff.';
if ~strcmp(subj,'t14')
    range.vision  = [params.vision.percentDiff(1), data.vision.max_signalDiff];
end
range.touch   = [params.touch.percentDiff(1), data.touch.max_signalDiff];

% run analyses and plot results
modes = {'vision', 'touch'};
for i = 1:2
    m = modes{i};
    f = figure('Name', [subj '_' m]);
    s.cols = 2;
    s.rows = 1;
    
    %subplot(s.rows, s.cols, 1)
    %text(0.2,0.5,m)
    %axis off
    
    if ~strcmp(subj,'t14')
        plotStaircase(f, s, 1, 'training', data.(m).staircase.signalDiffs, labels.(m), range.(m))
        plotStaircase(f, s, 2, 'main run', data.(m).main.signalDiffs, labels.(m), range.(m))
        diff.(m) = data.(m).main.signalDiffs;
    else
        range.vision = [0 .5];
        range.touch = [0 40];
        plotStaircase(f, s, 2, 'main run', data.(m).main.signalDiffs, labels.(m),range.(m))
        diff.(m) = data.(m).main.signalDiffs;
    end
end
end

%% plotStaircase
% name:         title of the subplot
% yLabel:       label for the y-axis
% yRange:       range for the y-axis
function plotStaircase(figHandle, subplotSet, position, name, signalDiffs, yLabel, yRange)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,position); hold on
title(name)
plot(signalDiffs)
xlabel('trial number'); ylabel(yLabel);
ylim(yRange)

end