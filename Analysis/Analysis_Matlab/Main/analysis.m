%% Main script

subjectNames = {'h1','h2','h3','h4','h5','h7','h8','h9','h10','h11','h12','h13','h14','h15','h16','h17','h18','h19', 'h20', 'h21', 'h22', 'h23', 'h24', 'h25'};   % Put all subjects to analyse in this cell array, HC6 did not fill in the questionnaires.
%subjectNames = {'t1','t2','t3','t4','t5','t6','t7','t8','t9','t10','t11','t12','t13','t14','t15','t16','t17','TS18','TS20', 't21', 't22', 't23', 't24', 't25', 't26', 't27'};   % Put all subjects to analyse in this cell array, TS19 has comorbid PD. 
%subjectNames = {'t16'};  


%subjectNames = {'h8'};

user = 'polina';

clear SSEfitAll
for i=1:numel(subjectNames)
    [nR_S1(i), nR_S2(i), SSEfitAll(i), SSEfit_2binsAll(i), MLEfitAll(i), MLEfit_2binsAll(i),...
        SSEfit_q(i), SSEfit_2bins_q(i), MLEfit_q(i), MLEfit_2bins_q(i),...
        corrPercAll(i), confMeanAll(i), confMedianAll(i), ...
        responseBiasAll(i), conf_All(i), conf_binnedAll(i), ...
        conf_quartilesAll(i), exclSkipped(i), exclRT(i)] = singleSubjectAnalysis(subjectNames{i}, user);
end

resFolder = '/Users/polinaarbuzova/Documents/TS study/Results';
cd(resFolder);

save('hc_hist_cprime_MLE_quant.mat')

%% singleSubjectAnalysis
% This runs a bunch of analyses on a subject's data and creates 2 figures:
% one for vision and one for touch.
%
% subj: name of the subject
% user: For each user, you need to add a case and define the dataPath
function [nR_S1, nR_S2, SSEfit, SSEfit_2bins, MLEfit, MLEfit_2bins, SSEfit_q, SSEfit_2bins_q, MLEfit_q, MLEfit_2bins_q, corr_perc, confMean, confMedian, responseBias, conf, conf_binned, conf_quartiles, exclSkipped, exclRT] = singleSubjectAnalysis(subj, user)

% data path and functions path
switch user
    case {'christina_home'}
        dataPath = 'C:\Users\christina\Dropbox\metacognition experiment\data';
    case {'polina'}
        dataPath = '/Users/polinaarbuzova/Documents/TS study/Data';     
end
functionPath = 'fx';
addpath(functionPath);

% load data
[params, data] = loadData(dataPath, subj);

% set labels and ranges
labels.vision = 'contrast diff.';
labels.touch  = 'vibration strength diff.';
if ~strcmp(subj,'t14')
range.vision  = [params.vision.percentDiff(1), data.vision.max_signalDiff];
end
range.touch   = [params.touch.percentDiff(1), data.touch.max_signalDiff];

% run analyses and plot results
modes = {'vision', 'touch'};
for i = 1:2
    m = modes{i};
    f = figure('Name', [subj '_' m]);
    s.cols = 4;
    s.rows = 3;

    %subplot(s.rows, s.cols, 1)
    %text(0.2,0.5,m)
    %axis off
    
    if ~strcmp(subj,'t14')
        plotStaircase(f, s, 1, 'training', data.(m).staircase.signalDiffs, labels.(m), range.(m))
        plotStaircase(f, s, 2, 'main run', data.(m).main.signalDiffs, labels.(m), range.(m))
    else
        range.vision = [0 .5];
        range.touch = [0 40];
        plotStaircase(f, s, 2, 'main run', data.(m).main.signalDiffs, labels.(m),range.(m))
    end
    
    [corr_perc.(m), responseBias.(m)] = plotType1(f, s, 4, data.(m).main.correct, data.(m).main.response);
    plotReactionTimes(f, s, 3, data.(m).main.RT, data.(m).main.correct)
    [nR_S1.(m), nR_S2.(m), conf.(m), conf_binned.(m), conf_quartiles.(m), SSEfit.(m), MLEfit.(m), nR_S1_q.(m), nR_S2_q.(m), SSEfit_q.(m), MLEfit_q.(m)] = metadPrime(f, s, [12 8], data.(m).main.conf, data.(m).main.strongDirection, data.(m).main.correct);
    [SSEfit_2bins.(m), MLEfit_2bins.(m), SSEfit_2bins_q.(m), MLEfit_2bins_q.(m)] = metadPrime_2bins(data.(m).main.conf, data.(m).main.strongDirection, data.(m).main.correct);
    checkConfBiasesLR(f, s, [5 9], data.(m).main.response, data.(m).main.conf)
    checkConfBiasesCI(f, s, [6 10], data.(m).main.correct, data.(m).main.conf)
    checkConfBins(f, s, [7 11], data.(m).main.conf, conf_binned.(m))
    confMean.(m) = mean(data.(m).main.conf);
    confMedian.(m) = median(data.(m).main.conf);
    exclSkipped.(m) = data.(m).main.excl_skipped;
    exclRT.(m) = data.(m).main.excl_RT; 
    
end

end

%% General remarks:
% For each of the functions below,
% - figHandle is the handle to the previously created figure you want to 
%   plot into.
% - subplotSet is a struct with fields rows (number of subplot rows) and
%   cols ( number of subplot columns).
% - position/positions is a number or an array of numbers that says which 
%   subplot(s) to plot into.
% - input parameters that aren't explained above the function are always
%   the data variables with that name

%% plotStaircase
% name:         title of the subplot
% yLabel:       label for the y-axis
% yRange:       range for the y-axis
function plotStaircase(figHandle, subplotSet, position, name, signalDiffs, yLabel, yRange)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,position); hold on
title(name)
plot(signalDiffs)
xlabel('trial number'); ylabel(yLabel);
ylim(yRange)

end

%% plotType1
% The first two bars in the plot are the proportions of correct and
% incorrect responses. Red lines indicate the values we tried to staircase
% (~71% correct, ~29% incorrect).
% The third and fourth bar are the proportions of left and right responses.
% Red lines indicate the unbiased values (50% each).
function [correct_perc, responseBias] = plotType1(figHandle, subplotSet, position, correct, response)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,position); hold on
title('type1 task')
correct_perc = sum(correct) / length(correct)*100;
bar([correct_perc, ...
     sum(~correct) / length(correct)*100, ...
     0, ...
     sum(response==1) / length(response)*100, ...
     sum(response==2) / length(response)*100]);
 

responseBias = (sum(response==1) / length(response)*100)/(sum(response==2) / length(response)*100);
 
set(gca, 'XTickLabel', {'correct', 'incorrect', ' ', 'left', 'right'}, 'XTick', 1:5) % 1=left, 2=right
plot([0.5 1.5], 100*[sqrt(0.5) sqrt(0.5)], 'r-')
plot([1.5 2.5], 100*[1-sqrt(0.5) 1-sqrt(0.5)], 'r-')
plot([3.5 5.5], [50 50], 'r-')
ylabel('% trials')

end

%% plotReactionTimes
% Two histograms for reaction times: one for correct trials, one for
% incorrect trials.
function plotReactionTimes(figHandle, subplotSet, position, RT, correct)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,position); hold on
title('type1 RT')
nbins = 30;
histogram(RT(logical(correct)), min(RT):range(RT)/nbins:max(RT), 'FaceColor', [0 .5 0]);
histogram(RT(logical(~correct)), min(RT):range(RT)/nbins:max(RT), 'FaceColor', [.5 0 0]);
legend('correct', 'incorrect')
xlabel('seconds')
ylabel('count')

end

%% metadPrime
% This does a metacognition analysis. Computes and plots nR_S1 and nR_S2 
% vectors. Computes and plots d', meta-d', M-ratio and M-difference. 
% Returns the binned confidence values.
% Nratings can be changed to use a different number of confidence bins.
function [nR_S1, nR_S2, conf, conf_binned, conf_quartiles, SSEfit, MLEfit, nR_S1_q, nR_S2_q, SSEfit_q, MLEfit_q]= metadPrime(figHandle, subplotSet, positions, conf, strongDirection, correct)

figure(figHandle)

Nratings = 6; %define how many confidence levels we want
[~, ~, conf_binned] = histcounts(conf,Nratings);

nR_S1 = NaN(1,2*Nratings);
nR_S2 = NaN(1,2*Nratings);
for i = 1:Nratings
    nR_S1(i)            = sum(correct & strongDirection == 1 & conf_binned == Nratings+1-i);
    nR_S1(Nratings+i)   = sum(~correct & strongDirection == 1 & conf_binned == i);
    
    nR_S2(Nratings+i)   = sum(correct & strongDirection == 2 & conf_binned == i);
    nR_S2(i)            = sum(~correct & strongDirection == 2 & conf_binned == Nratings+1-i);
end

SSEfit = type2_SDT_SSE(nR_S1, nR_S2);
MLEfit = type2_SDT_MLE(nR_S1, nR_S2);

%possible alternative: 
conf_quartiles = quantileranks(conf, Nratings);
nR_S1_q = NaN(1,2*Nratings);
nR_S2_q = NaN(1,2*Nratings);
for i = 1:Nratings
    nR_S1_q(i)            = sum(correct & strongDirection == 1 & conf_quartiles == Nratings+1-i);
    nR_S1_q(Nratings+i)   = sum(~correct & strongDirection == 1 & conf_quartiles == i);
    
    nR_S2_q(Nratings+i)   = sum(correct & strongDirection == 2 & conf_quartiles == i);
    nR_S2_q(i)            = sum(~correct & strongDirection == 2 & conf_quartiles == Nratings+1-i);
end

SSEfit_q = type2_SDT_SSE(nR_S1_q, nR_S2_q);
MLEfit_q = type2_SDT_MLE(nR_S1_q, nR_S2_q);



figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,positions(1)); hold on
title('Meta-d analysis')
bar([SSEfit.meta_da, SSEfit.da, SSEfit.M_ratio, SSEfit.M_diff]);
set(gca, 'XTickLabel', {'Meta-d', 'd', 'Mratio', 'Mdiff'}, 'XTick', 1:4)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,positions(2)); hold on
title('nR_{S1} & nR_{S2}')
bar([nR_S1' nR_S2']);
legend('nR_{S1}', 'nR_{S2}')
set(gca, 'XTickLabel', {'r1c3', 'r1c2', 'r1c1', 'r2c1', 'r2c2', 'r2c3'}, 'XTick', 1:6)
xlabel('combined response')
ylabel('count')


end
%% metadPrime-2
% This does a metacognition analysis. Computes and plots nR_S1 and nR_S2 
% vectors. Computes and plots d', meta-d', M-ratio and M-difference. 
% Returns the binned confidence values.
% Nratings can be changed to use a different number of confidence bins.->
% And here we use two bins only to exclude based on SDT-
function [SSEfit_2bins, MLEfit_2bins, SSEfit_2bins_q, MLEfit_2bins_q] = metadPrime_2bins(conf, strongDirection, correct)

Nratings = 2; %define how many confidence levels we want
[~, ~, conf_binned] = histcounts(conf,Nratings);
%possible alternative: 
conf_quartiles = quantileranks(conf, Nratings);

nR_S1 = NaN(1,2*Nratings);
nR_S2 = NaN(1,2*Nratings);
for i = 1:Nratings
    nR_S1(i)            = sum(correct & strongDirection == 1 & conf_binned == Nratings+1-i);
    nR_S1(Nratings+i)   = sum(~correct & strongDirection == 1 & conf_binned == i);
    
    nR_S2(Nratings+i)   = sum(correct & strongDirection == 2 & conf_binned == i);
    nR_S2(i)            = sum(~correct & strongDirection == 2 & conf_binned == Nratings+1-i);
end

SSEfit_2bins = type2_SDT_SSE_2bins(nR_S1, nR_S2);
MLEfit_2bins = type2_SDT_MLE(nR_S1, nR_S2);

% MLE
for i = 1:Nratings
    nR_S1_q(i)            = sum(correct & strongDirection == 1 & conf_binned == Nratings+1-i);
    nR_S1_q(Nratings+i)   = sum(~correct & strongDirection == 1 & conf_binned == i);
    
    nR_S2_q(Nratings+i)   = sum(correct & strongDirection == 2 & conf_binned == i);
    nR_S2_q(i)            = sum(~correct & strongDirection == 2 & conf_binned == Nratings+1-i);
end
SSEfit_2bins_q = type2_SDT_SSE_2bins(nR_S1, nR_S2);
MLEfit_2bins_q = type2_SDT_MLE(nR_S1, nR_S2);


end



%% checkConfBiasesLR
% Goal: Check if the type 1 choice (left or right) biases confidence
% judgements (lower or higher).
% Makes two plots: One confidence histogram for left responses, one for
% right responses.
% Displays the mean confidence for left and right responses.
% The number of bins can be adjusted in nbins.
function checkConfBiasesLR(figHandle, subplotSet, positions, response, conf)

conf1 = conf(response==1);
conf2 = conf(response==2);

figure(figHandle)
nbins = 10;
subplot(subplotSet.rows,subplotSet.cols,positions(1)); hold on
histogram(conf1, 0:1/nbins:1, 'FaceColor', [.5 .5 0]);
title('left')
xlabel('confidence')
ylabel('count')
subplot(subplotSet.rows,subplotSet.cols,positions(2))
histogram(conf2, 0:1/nbins:1, 'FaceColor', [0 0 .5]);
title('right')
xlabel('confidence')
ylabel('count')

disp('mean left')
disp(mean(conf1(~isnan(conf1))))
disp('mean right')
disp(mean(conf2(~isnan(conf2))))
end

%% checkConfBiasesCI
% Goal: Check if the correctness of the type 1 choice (correct or 
% incorrect) biases confidence judgements (lower or higher). - With good
% metacognition, it should: Correct responses should on average be 
% correlated with higher confidence.
% Makes two plots: One confidence histogram for correct responses, one for
% incorrect responses.
% Displays the mean confidence for correct and incorrect responses.
% The number of bins can be adjusted in nbins.
function checkConfBiasesCI(figHandle, subplotSet, positions, correct, conf)

conf1 = conf(correct==1);
conf2 = conf(correct==0);

figure(figHandle)
nbins = 10;
subplot(subplotSet.rows,subplotSet.cols,positions(1)); hold on
histogram(conf1, 0:1/nbins:1, 'FaceColor', [.5 .5 0]);
title('correct')
xlabel('confidence')
ylabel('count')
subplot(subplotSet.rows,subplotSet.cols,positions(2))
histogram(conf2, 0:1/nbins:1, 'FaceColor', [0 0 .5]);
title('incorrect')
xlabel('confidence')
ylabel('count')

disp('mean correct')
disp(mean(conf1(~isnan(conf1))))
disp('mean incorrect')
disp(mean(conf2(~isnan(conf2))))
end

%% checkConfBins
% conf_binned: binned confidence values as returned by quantileranks
% Makes two histogram plots: One with the original confidence values before
% binning (number of histogram bins can be adjusted in nbins) and one with 
% the binned confidence values.
function checkConfBins(figHandle, subplotSet, positions, conf, conf_binned)

figure(figHandle)
nbins = 10;
subplot(subplotSet.rows,subplotSet.cols,positions(1)); hold on
histogram(conf, 0:1/nbins:1, 'FaceColor', [.5 .5 0]);
title('original')
xlabel('confidence')
ylabel('count')
subplot(subplotSet.rows,subplotSet.cols,positions(2))
histogram(conf_binned, 'FaceColor', [0 0 .5]);
title('binned')
xlabel('confidence')
ylabel('count')

end