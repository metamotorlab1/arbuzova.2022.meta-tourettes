% Polina Arbuzova, May 2020
%% Check staircases in TS study and variability of difficulty
%close all
clear all

resFolder = '/Users/polinaarbuzova/Documents/TS study/Results';
cd(resFolder);

load('hc_staircases_fullDataset.mat');

nsubj = 24;
rows = 2;
columns = 1;
nbins = 6;

labels.vision = 'contrast diff.';
labels.touch  = 'vibration strength diff.';

hc_sd_diff_vis = [];
hc_sd_diff_touch = [];
hc_mean_diff_vis = [];
hc_mean_diff_touch = [];
hc_sd_ratio_diff_vis = [];
hc_sd_ratio_diff_touch = [];
for s = 1:nsubj
    hc_sd_diff_vis = [hc_sd_diff_vis std(staircases_All(s).vision)];
    hc_sd_diff_touch = [hc_sd_diff_touch std(staircases_All(s).touch)];
    
    hc_mean_diff_vis = [hc_mean_diff_vis mean(staircases_All(s).vision)];
    hc_mean_diff_touch = [hc_mean_diff_touch mean(staircases_All(s).touch)];
    
    hc_sd_ratio_diff_vis = [hc_sd_ratio_diff_vis hc_mean_diff_vis(s)/hc_sd_diff_vis(s)]
    hc_sd_ratio_diff_touch = [hc_sd_ratio_diff_touch hc_mean_diff_touch(s)/hc_sd_diff_touch(s)]
    
end
hc_sd_diff_vis_mean = mean(hc_sd_diff_vis);
hc_sd_diff_touch_mean = mean(hc_sd_diff_touch);

hc_mean_diff_vis_mean = mean(hc_mean_diff_vis);
hc_mean_diff_touch_mean = mean(hc_sd_diff_touch);

for s = 1:nsubj
    
    figure(1);
    subplot(rows,columns,1); hold on
    plot(staircases_All(s).vision)
    xlabel('trial number');
    ylim([0 0.4])
    title([num2str(s) 'HC vis, SD ' num2str(hc_sd_diff_vis_mean)])
    ylabel(labels.vision)
    
    figure(1);
    subplot(rows,columns,2); hold on
    plot(staircases_All(s).touch)
    xlabel('trial number');
    ylim([0 50])
    title([num2str(s) 'HC touch, SD ' num2str(hc_sd_diff_touch_mean)])
    ylabel(labels.touch)
end

load('ts_staircases_fullDataset.mat')
nsubj = 26;
rows = 2;
columns = 1;
nbins = 6;


ts_sd_diff_vis = [];
ts_sd_diff_touch = [];
ts_mean_diff_vis = [];
ts_mean_diff_touch = [];
ts_sd_ratio_diff_vis = [];
ts_sd_ratio_diff_touch = [];
for s = 1:nsubj
    ts_sd_diff_vis = [ts_sd_diff_vis std(staircases_All(s).vision)]
    ts_sd_diff_touch = [ts_sd_diff_touch std(staircases_All(s).touch)]
       
    ts_mean_diff_vis = [ts_mean_diff_vis mean(staircases_All(s).vision)]
    ts_mean_diff_touch = [ts_mean_diff_touch mean(staircases_All(s).touch)]
    
    ts_sd_ratio_diff_vis = [ts_sd_ratio_diff_vis ts_mean_diff_vis(s)/ts_sd_diff_vis(s)]
    ts_sd_ratio_diff_touch = [ts_sd_ratio_diff_touch ts_mean_diff_touch(s)/ts_sd_diff_touch(s)]
end

ts_sd_diff_vis_mean = mean(ts_sd_diff_vis);
ts_sd_diff_touch_mean = mean(ts_sd_diff_touch);

for s = 1:nsubj
       
    figure(2);
    subplot(rows,columns,1); hold on
    plot(staircases_All(s).vision)
    xlabel('trial number');
    ylim([0 0.4])
    title([num2str(s) 'TS vis, SD ' num2str(ts_sd_diff_vis_mean)])
    ylabel(labels.vision)
    
    figure(2);
    subplot(rows,columns,2); hold on
    plot(staircases_All(s).touch)
    xlabel('trial number');
    ylim([0 50])
    title([num2str(s) 'TS touch, SD ' num2str(ts_sd_diff_touch_mean)])
    ylabel(labels.touch)
end

[h,p,ci,stats] = ttest2(hc_sd_diff_vis, ts_sd_diff_vis)
[h,p,ci,stats] = ttest2(hc_sd_diff_touch, ts_sd_diff_touch)


[h,p,ci,stats] = ttest2(hc_mean_diff_vis, ts_mean_diff_vis)
[h,p,ci,stats] = ttest2(hc_mean_diff_touch, ts_mean_diff_touch)

% Exclude what is excluded for the m-ratio analysis
hc_mratios = load('hc_hist_mratios_new_excluded13.mat')
ts_mratios = load('ts_hist_mratios_new_excluded13.mat')

% Mean
[h,p,ci,stats] = ttest2(hc_mean_diff_vis(hc_mratios.idx_clean_vis), ts_mean_diff_vis(ts_mratios.idx_clean_vis))
[h,p,ci,stats] = ttest2(hc_mean_diff_touch(hc_mratios.idx_clean_touch), ts_mean_diff_touch(ts_mratios.idx_clean_touch))

mean(hc_mean_diff_vis(hc_mratios.idx_clean_vis))
std(hc_mean_diff_vis(hc_mratios.idx_clean_vis))

mean(ts_mean_diff_vis(ts_mratios.idx_clean_vis))
std(ts_mean_diff_vis(ts_mratios.idx_clean_vis))


mean(hc_mean_diff_touch(hc_mratios.idx_clean_touch))
std(hc_mean_diff_touch(hc_mratios.idx_clean_touch))

mean(ts_mean_diff_touch(ts_mratios.idx_clean_touch))
std(ts_mean_diff_touch(ts_mratios.idx_clean_touch))

% SD
[h,p,ci,stats] = ttest2(hc_sd_diff_vis(hc_mratios.idx_clean_vis), ts_sd_diff_vis(ts_mratios.idx_clean_vis))
[h,p,ci,stats] = ttest2(hc_sd_diff_touch(hc_mratios.idx_clean_touch), ts_sd_diff_touch(ts_mratios.idx_clean_touch))

mean(hc_sd_diff_vis(hc_mratios.idx_clean_vis))
std(hc_sd_diff_vis(hc_mratios.idx_clean_vis))

mean(ts_sd_diff_vis(ts_mratios.idx_clean_vis))
std(ts_sd_diff_vis(ts_mratios.idx_clean_vis))


mean(hc_sd_diff_touch(hc_mratios.idx_clean_touch))
std(hc_sd_diff_touch(hc_mratios.idx_clean_touch))

mean(ts_sd_diff_touch(ts_mratios.idx_clean_touch))
std(ts_sd_diff_touch(ts_mratios.idx_clean_touch))

% Check SDs

[h,p,ci,stats] = ttest2(hc_sd_ratio_diff_vis(hc_mratios.idx_clean_vis), ts_sd_ratio_diff_vis(ts_mratios.idx_clean_vis))
[h,p,ci,stats] = ttest2(hc_sd_ratio_diff_touch(hc_mratios.idx_clean_touch), ts_sd_ratio_diff_touch(ts_mratios.idx_clean_touch))

mean(hc_sd_ratio_diff_vis(hc_mratios.idx_clean_vis))
std(hc_sd_ratio_diff_vis(hc_mratios.idx_clean_vis))

mean(ts_sd_ratio_diff_vis(ts_mratios.idx_clean_vis))
std(ts_sd_ratio_diff_vis(ts_mratios.idx_clean_vis))


mean(hc_sd_ratio_diff_touch(hc_mratios.idx_clean_touch))
std(hc_sd_ratio_diff_touch(hc_mratios.idx_clean_touch))

mean(ts_sd_ratio_diff_touch(ts_mratios.idx_clean_touch))
std(ts_sd_ratio_diff_touch(ts_mratios.idx_clean_touch))

% ... within group t-tests
[h,p,ci,stats] = ttest(hc_sd_ratio_diff_vis(hc_mratios.idx_clean), hc_sd_ratio_diff_touch(hc_mratios.idx_clean))
[h,p,ci,stats] = ttest(ts_sd_ratio_diff_vis(ts_mratios.idx_clean), ts_sd_ratio_diff_touch(ts_mratios.idx_clean))

% For each subject, we determined the level of experienced stimulus variability (computed as the ratio of the SD across all experienced contrasts and the average contrast).
% (Rahnev & Fleming, 2019: https://academic.oup.com/nc/article/2019/1/niz009/5513064)

% ... between groups
[h,p,ci,stats] = ttest2(hc_sd_ratio_diff_vis(hc_mratios.idx_clean_vis), ts_sd_ratio_diff_vis(ts_mratios.idx_clean_vis))
[h,p,ci,stats] = ttest2(hc_sd_ratio_diff_touch(hc_mratios.idx_clean_touch), ts_sd_ratio_diff_touch(ts_mratios.idx_clean_touch))
