# Arbuzova.2022.Meta-Tourettes

Material to accompany the article "No evidence of impaired visual and tactile metacognition in adults with tourette disorder" by Arbuzova et al, 2022. https://doi.org/10.1016/j.parkreldis.2022.02.019


Copied all materials from https://osf.io/3djxs/

