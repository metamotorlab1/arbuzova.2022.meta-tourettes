% Polina Arbuzova, May 2020
% Correlate difference in m-ratios between tactile in TS
% with clinical scores

cd('/Users/polinaarbuzova/Documents/TS study/Results');
jaspDir = '/Users/polinaarbuzova/Documents/TS study/Results/JASP results';
load('ts_hist_mratios_new.mat');
addpath(genpath('/Users/polinaarbuzova/Dropbox/PhDs/Polina/MatLab Toolboxes/Corr_toolbox_v2'));


puts = [27
8 
27
33
NaN
23
29
26
32
28
24
25
10
21
28
29
30
20
9
29
27
28
15
21
22
19];

ygtss = [77
66
54
75
24
48
55
36
82
45
21
65
33
22
56
60
51
28
67
34
77
48
6
59
42
91];


% PUTS
putsClean = puts(idx_clean_touch)';
puts_idx_clean = find(~isnan(putsClean));
idx_super_clean = idx_clean_touch(puts_idx_clean);
putsClean = putsClean(puts_idx_clean);

% YGTSS
ygtssClean = ygtss(idx_clean_touch)';

% Just tactile
touch_clean_puts = touch.mratio(idx_super_clean);
touch_clean_ygtss = touch.mratio(idx_clean_touch);
puts_corr = robust_correlation(touch_clean_puts, putsClean);
ygtss_corr = robust_correlation(touch_clean_ygtss, ygtssClean);


%%
% Find the bivariate outliers (in skipped correlations, boxplots rule is
% used for that)
% PUTS
outliers_idx = find(puts_corr.outliers.bivariate.boxplot==1);
touch_clean_puts(outliers_idx)=[];
putsClean(outliers_idx)=[];

Bayes_puts = table;
Bayes_puts.puts = putsClean';
Bayes_puts.mratio = touch_clean_puts';

% Save it as a CSV
cd(jaspDir);
writetable(Bayes_puts,'Bayes_puts_new.csv')

% Get the outliers
outliers_idx = find(puts_corr.outliers.bivariate.boxplot==1);

puts_outliers = putsClean(outliers_idx);
mratio_puts = touch_clean_puts(outliers_idx);

outliers_V_M = table;
outliers_V_M.visual = Skittles_v_m_visual_outliers;
outliers_V_M.motor = Skittles_v_m_motor_outliers;
cd(outliersDir);
writetable(outliers_V_M,'outliers_V_M.csv') 



% YGTSS

outliers_idx = find(ygtss_corr.outliers.bivariate.boxplot==1);
touch_clean_ygtss(outliers_idx)=[];
ygtssClean(outliers_idx)=[];

Bayes_ygtss = table;
Bayes_ygtss.ygtss = ygtssClean';
Bayes_ygtss.mratio = touch_clean_ygtss';

% Save it as a CSV
cd(jaspDir);
writetable(Bayes_ygtss,'Bayes_ygtss_new_new.csv')






