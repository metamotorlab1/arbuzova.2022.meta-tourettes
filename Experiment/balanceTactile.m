clear all
KbName('UnifyKeyNames');
clearvib=instrfind();
if ~isempty(clearvib)
    fclose(clearvib);
    delete(clearvib)
end
%%

myPath = pwd;
addpath('functions')

commandwindow;

%dlg = inputdlg({'Name' 'TopOrBottom'},'Input');
dlg = inputdlg({'Name'},'Input');
name                = dlg{1};
TopOrBottom         = 1; %str2double(dlg{2});
saveDir         = [myPath filesep 'data' filesep 'subj' name '_balanceTactile_ToporBottom' num2str(TopOrBottom)];

if ~isdir(saveDir), mkdir(saveDir),end


%% set up parameters
P.stimDuration          = 0.2;                                             % duration of stimuli (seconds)

P.baseVibration         = 90;
P.stepSize              = 2;

%% keys
P.leftAnswerKey     = KbName('LeftArrow');
P.rightAnswerKey    = KbName('RightArrow');

%% set up tactile stimuli

port_vibrator='COM5';
vibrator = serial(port_vibrator,'Baud',115200);
vibrator.InputBufferSize=256;
fopen(vibrator)

switch TopOrBottom
    case 1 % top
        balance = [1 1 0 0];
    case 2 % bottom
        balance = [0 0 1 1];
end


%% start the staircase with the assumption that balance = [.5 .5]

fprintf('\n\n')
fprintf('Press right and left arrows to indicate where the strongest vibration came from. \n')
fprintf('Press any key to start \n')
pause;

happy   = 0;
t       = 1;

while ~happy
    
    stim=P.baseVibration*(abs(balance)>0)+balance;
    %fprintf(vibrator,sprintf('%da%da%da%da',stim));
    fprintf(vibrator,sprintf('s%03da%03da%03da%03d',stim(1),stim(2),stim(3),stim(4)));
    WaitSecs(P.stimDuration);
    fprintf(vibrator,sprintf('s%03da%03da%03da%03d',0,0,0,0));
    
    %% get response
    
    RestrictKeysForKbCheck([P.leftAnswerKey P.rightAnswerKey]);
    keyIsDown = 0;
    while keyIsDown == 0 %if the key is not down, go into the loop
        [keyIsDown, Secs, keyCode] = KbCheck; %read the keyboard
    end
    
    
    %do a simple staircase
    %If left is reported to be stronger, then reduce left, increase right.
    if  keyCode(P.leftAnswerKey)
        switch TopOrBottom
            case 1 % top
                balance = balance + P.stepSize * [-1 1 0 0];
            case 2 % bottom
                balance = balance + P.stepSize * [0 0 -1 1];
        end
    elseif keyCode(P.rightAnswerKey)
        switch TopOrBottom
            case 1 % top
                balance = balance + P.stepSize * [1 -1 0 0];
            case 2 % bottom
                balance = balance + P.stepSize * [0 0 1 -1];
        end
    end
    Data.answerLeft(t)  = keyCode(P.leftAnswerKey);
    
    reversals = diff(Data.answerLeft);
    Data.balance(t,:)  = balance;
    
    
    %get a criterion for happiness
    %remember that diff will always give a vector of length n-1
    if t > 10 && sum(abs(reversals(t-8:t-1))) > 6
        happy = 1;
    end
    t = t+1;
    KbWait([],1);
    WaitSecs(.5);

end
switch TopOrBottom
    case 1 % top
        Resu_balanceTactile = [stim(1)/(stim(1)+stim(2)) stim(2)/(stim(1)+stim(2))];
    case 2 % bottom
        Resu_balanceTactile = [stim(3)/(stim(3)+stim(4)) stim(4)/(stim(3)+stim(4))];
end

save([saveDir filesep 'alldat_subj' name '_balancetactile.mat']);

%then, we just open this in the experimental file and replace the
%Data.balance in the place where the totalSound is calculated

fprintf('Done! \n')

figure;
switch TopOrBottom
    case 1 % top
        figure;
        plot(Data.balance(:,1),'+-')
        legend('balance top left')
        figure;
        plot(Data.balance(:,2),'+-')
        legend('balance top right')
    case 2 % bottom
        figure;
        plot(Data.balance(:,3),'+-')
        legend('balance bottom left')
        figure;
        plot(Data.balance(:,4),'+-')
        legend('balance bottom right')
end

%hold on
%plot(reversals,'r*')
