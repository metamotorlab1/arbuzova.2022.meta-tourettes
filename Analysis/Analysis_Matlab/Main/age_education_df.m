% Polina Arbuzova, July 2021
% Metacognition in Tourette project

% Age and years of education

subjectNamesHC = {'h1';'h2';'h3';'h4';'h5';'h7';'h8';'h9';'h10';'h11';'h12';'h13';'h14';'h15';'h16';'h17';'h18';'h19'; 'h20'; 'h21'; 'h22'; 'h23'; 'h24'; 'h25'};   
subjectNamesTS = {'t1';'t2';'t3';'t4';'t5';'t6';'t7';'t8';'t9';'t10';'t11';'t12';'t13';'t14';'t15';'t16';'t17';'TS18';'TS20'; 't21'; 't22'; 't23'; 't24'; 't25'; 't26'; 't27'};   

hc_edu = [14.50; 14.50; 14.50; 25; 16; 23; 19; 16; 20; 24; 13; 18; 18; 15; 13; 25; 19; 17; 18; 17; 21; 16; 14.5; 23];   
ts_edu = [13; 21.50; 17; 18; 25; 10; 15; 13; 20; 15; 16; 16.50; 15; 18; 12; 19; 10; 18; 11; 20; 16; 16; 23; 17; 18; 21];   

hc_age = [22; 21; 21; 31; 26; 29; 29; 25; 29; 31; 28; 27; 27; 25; 25; 35; 28; 23; 51; 23; 34; 42; 31; 43];
ts_age = [24; 31; 22; 26; 57; 45; 26; 18; 40; 22; 24; 22; 24; 24; 19; 28; 26; 32; 19; 28; 42; 27; 29; 30; 33; 35];   

subject = [subjectNamesHC; subjectNamesTS];
education = [hc_edu; ts_edu];
age = [hc_age; ts_age];

tsStudy = table(subject, education, age)
writetable(tsStudy,'ts_study_age_education.csv') 

[h,p,ci,stats] = ttest2(hc_edu, ts_edu)

mean(hc_edu)
std(hc_edu)
mean(ts_edu)
std(ts_edu)