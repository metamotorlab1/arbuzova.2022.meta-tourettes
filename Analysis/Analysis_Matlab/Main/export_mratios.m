% (c) Polina Arbuzova, Jan 2021
% Let's compare mean d' 
% with participants excluded based on the top high (>10%) skipped trials
% rate


cd('/Users/polinaarbuzova/Documents/TS study/Results');

ts = load('ts_hist_mratios_rt_exl13_MLE_quant.mat');
hc = load('hc_hist_mratios_rt_exl13_MLE_quant.mat');

% [h,p,ci,stats] = ttest2(ts.vis.da(ts.idx_clean_vis), hc.vis.da(hc.idx_clean_vis))
% [h,p,ci,stats] = ttest2(ts.touch.da(ts.idx_clean_touch), hc.touch.da(hc.idx_clean_touch))
% 
% [h,p,ci,stats] = ttest2(ts.vis.meta_da(ts.idx_clean_vis), hc.vis.meta_da(hc.idx_clean_vis))
% [h,p,ci,stats] = ttest2(ts.touch.meta_da(ts.idx_clean_touch), hc.touch.meta_da(hc.idx_clean_touch))
% 
% [h,p,ci,stats] = ttest2(ts.vis.mratio(ts.idx_clean_vis), hc.vis.mratio(hc.idx_clean_vis))
% [h,p,ci,stats] = ttest2(ts.touch.mratio(ts.idx_clean_touch), hc.touch.mratio(hc.idx_clean_touch))

% Put together for ANOVA
% With NaNs - M-RATIOS
mratios.hc.touch = hc.touch.mratio';
mratios.hc.vis = hc.vis.mratio';
mratios.ts.touch = ts.touch.mratio';
mratios.ts.vis = ts.vis.mratio';

% With NaNs  - META_DA
meta_da.hc.touch = hc.touch.meta_da';
meta_da.hc.vis = hc.vis.meta_da';
meta_da.ts.touch = ts.touch.meta_da';
meta_da.ts.vis = ts.vis.meta_da';

% With NaNs - DA
da.hc.touch = hc.touch.da';
da.hc.vis = hc.vis.da';
da.ts.touch = ts.touch.da';
da.ts.vis = ts.vis.da';

% With NaNs - CONF-MEAN
confMean.hc.touch = hc.touch.confMean';
confMean.hc.vis = hc.vis.confMean';
confMean.ts.touch = ts.touch.confMean';
confMean.ts.vis = ts.vis.confMean';

% With NaNs - CONF-MED
confMed.hc.touch = hc.touch.confMed';
confMed.hc.vis = hc.vis.confMed';
confMed.ts.touch = ts.touch.confMed';
confMed.ts.vis = ts.vis.confMed';

%%% MLE
% With NaNs - M-RATIOS
mratios_MLE.hc.touch = hc.touch.mratio_MLE';
mratios_MLE.hc.vis = hc.vis.mratio_MLE';
mratios_MLE.ts.touch = ts.touch.mratio_MLE';
mratios_MLE.ts.vis = ts.vis.mratio_MLE';

% With NaNs  - META_DA
meta_da_MLE.hc.touch = hc.touch.meta_da_MLE';
meta_da_MLE.hc.vis = hc.vis.meta_da_MLE';
meta_da_MLE.ts.touch = ts.touch.meta_da_MLE';
meta_da_MLE.ts.vis = ts.vis.meta_da_MLE';

% With NaNs - DA
da_MLE.hc.touch = hc.touch.da_MLE';
da_MLE.hc.vis = hc.vis.da_MLE';
da_MLE.ts.touch = ts.touch.da_MLE';
da_MLE.ts.vis = ts.vis.da_MLE';

%%%%% Quantiles %%%%%
% With NaNs - M-RATIOS
mratios_q.hc.touch = hc.touch_q.mratio';   
mratios_q.hc.vis = hc.vis_q.mratio';
mratios_q.ts.touch = ts.touch_q.mratio';
mratios_q.ts.vis = ts.vis_q.mratio';

%%% MLE
% With NaNs - M-RATIOS
mratios_q_MLE.hc.touch = hc.touch_q_mle.mratio_MLE';
mratios_q_MLE.hc.vis = hc.vis_q_mle.mratio_MLE';
mratios_q_MLE.ts.touch = ts.touch_q_mle.mratio_MLE';
mratios_q_MLE.ts.vis = ts.vis_q_mle.mratio_MLE';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add condition column
hc_modality_touch = cell(length(meta_da.hc.touch), 1);
hc_modality_touch(:) = {'tactile'};

hc_modality_vis = cell(length(meta_da.hc.vis), 1);
hc_modality_vis(:) = {'visual'};

ts_modality_touch = cell(length(meta_da.ts.touch), 1);
ts_modality_touch(:) = {'tactile'};

ts_modality_vis = cell(length(meta_da.ts.vis), 1);
ts_modality_vis(:) = {'visual'};

% Add group column
hc_group_touch = cell(length(meta_da.hc.touch), 1);
hc_group_touch(:) = {'HC'};

hc_group_vis = cell(length(meta_da.hc.vis), 1);
hc_group_vis(:) = {'HC'};

ts_group_touch = cell(length(meta_da.ts.touch), 1);
ts_group_touch(:) = {'TS'};

ts_group_vis = cell(length(meta_da.ts.vis), 1);
ts_group_vis(:) = {'TS'};

% Add ID
hc_id = [(1:1:length(meta_da.hc.touch))';  (1:1:length(meta_da.hc.vis))'];
ts_id = [(1:1:length(meta_da.ts.touch))';  (1:1:length(meta_da.ts.vis))'];
ts_id = ts_id+max(hc_id);
id = [hc_id; ts_id]
% group, modality, mratio (subject?)
group = [hc_group_touch; hc_group_vis; ts_group_touch; ts_group_vis];
modality = [hc_modality_touch; hc_modality_vis; ts_modality_touch; ts_modality_vis];
mratio = [mratios.hc.touch; mratios.hc.vis; mratios.ts.touch; mratios.ts.vis];
metada = [meta_da.hc.touch; meta_da.hc.vis; meta_da.ts.touch; meta_da.ts.vis];
dprime =  [da.hc.touch; da.hc.vis; da.ts.touch; da.ts.vis];
confMn = [confMean.hc.touch; confMean.hc.vis; confMean.ts.touch; confMean.ts.vis];
confMd = [confMed.hc.touch; confMed.hc.vis; confMed.ts.touch; confMed.ts.vis];
%+MLE
mratio_mle = [mratios_MLE.hc.touch; mratios_MLE.hc.vis; mratios_MLE.ts.touch; mratios_MLE.ts.vis];
metada_mle = [meta_da_MLE.hc.touch; meta_da_MLE.hc.vis; meta_da_MLE.ts.touch; meta_da_MLE.ts.vis];
dprime_mle =  [da_MLE.hc.touch; da_MLE.hc.vis; da_MLE.ts.touch; da_MLE.ts.vis];
%+quantiles
mratio_q = [mratios_q.hc.touch; mratios_q.hc.vis; mratios_q.ts.touch; mratios_q.ts.vis];
mratio_q_mle = [mratios_q_MLE.hc.touch; mratios_q_MLE.hc.vis; mratios_q_MLE.ts.touch; mratios_q_MLE.ts.vis];


% Put together in a table
tsStudy = table(id, group, modality, mratio, metada, dprime, confMn, confMd, mratio_mle, metada_mle, dprime_mle, mratio_q, mratio_q_mle)
writetable(tsStudy,'sdt_measures_hist_long_withNaNs_excl13_MLE_quant.csv') 