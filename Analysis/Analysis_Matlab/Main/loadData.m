function [params, data] = loadData3(dataPath, subj)

% load data and parameters from all parts of the experiment
[~,             data.vision.staircase] = loadPart(dataPath, subj, {'vision', 'staircase'});
[params.vision, data.vision.main]      = loadPart(dataPath, subj, {'vision', 'main'});
[~,             data.touch.staircase]  = loadPart(dataPath, subj, {'touch',  'staircase'});
[params.touch,  data.touch.main]       = loadPart(dataPath, subj, {'touch',  'main'});

% compute additional useful values
data.vision.min_signalDiff = min(min(data.vision.staircase.signalDiffs),min(data.vision.main.signalDiffs));
data.vision.max_signalDiff = max(max(data.vision.staircase.signalDiffs),max(data.vision.main.signalDiffs));
data.touch.min_signalDiff = min(min(data.touch.staircase.signalDiffs),min(data.touch.main.signalDiffs));
data.touch.max_signalDiff = max(max(data.touch.staircase.signalDiffs),max(data.touch.main.signalDiffs));

% Filter out 5% slowest trials per participant and per task (as suggested
% by Nathan)
data.vision.RT_95_thresh = [];
data.touch.RT_95_thresh = [];
data.vision.RT_95_thresh = prctile(data.vision.main.RT,95);
data.touch.RT_95_thresh = prctile(data.touch.main.RT,95);

ex_RT.vision = find(data.vision.main.RT>data.vision.RT_95_thresh);
ex_RT.touch = find(data.touch.main.RT>data.touch.RT_95_thresh);

data.vision.main.RT(ex_RT.vision) = [];
data.vision.main.correct(ex_RT.vision) = [];
data.vision.main.response(ex_RT.vision) = [];
data.vision.main.conf(ex_RT.vision) = [];
data.vision.main.strongDirection(ex_RT.vision) = [];

data.touch.main.RT(ex_RT.touch) = [];
data.touch.main.correct(ex_RT.touch) = [];
data.touch.main.response(ex_RT.touch) = [];
data.touch.main.conf(ex_RT.touch) = [];
data.touch.main.strongDirection(ex_RT.touch) = [];

end

%% loadPart
% part: cell array, part{1}='vision' or 'touch', part{2}='staircase' or 'main'
function [params, data] = loadPart(dataPath, subj, part)

if strcmp(part{1},'vision')
    v = '1';
    t = '0';
elseif strcmp(part{1},'touch')
    v = '0';
    t = '1';
end

if strcmp(part{2},'staircase')
    s = '1';
    c = 0;
elseif strcmp(part{2},'main')
    s = '0';
    c = 1;
end

oldPath = pwd;
cd([dataPath filesep 'subj' subj '_audio0_visual' v '_tactile' t '_staircase' s '_train0'])

if ~strcmp(subj, 't22')
    fullDataFile = dir('alldat*.mat');
    load(fullDataFile.name);
end

if strcmp(subj,'t22') % this participants doesn't have results saved in the same dataframe as others, hence this workaround
    if v == '0'
        if t == '1'
            if s == '0'
                fullDataFile = 'trialdat_t22_block4.mat';
                load(fullDataFile);
            else
                fullDataFile = dir('alldat*.mat');
                load(fullDataFile.name);
            end
        else
            fullDataFile = dir('alldat*.mat');
            load(fullDataFile.name);
        end
    else
        fullDataFile = dir('alldat*.mat');
        load(fullDataFile.name);
    end
else
    fullDataFile = dir('alldat*.mat');
    load(fullDataFile.name);
end



cd(oldPath)

params = cfg.(part{1});

data.signalDiffs     = zeros(1,length(Info.T));
data.strongDirection = zeros(1,length(Info.T));
if c, data.conf      = zeros(1,length(Info.T)); end

% if ~strcmp(subj,'t14')
%     n_trials = length([Info.T.RT]);
% elseif strcmp(part{1},'vision')
%     n_trials = 3;
% else
%     
% end

n_trials = length([Info.T.RT]);

for tr = 1:n_trials
    data.signalDiffs(tr)     = Info.T(tr).(part{1}).signalDiff;
    data.strongDirection(tr) = Info.T(tr).(part{1}).strongDirection;
    if c, data.conf(tr)      = Info.T(tr).VAS.confidence; end
end
data.RT      = [Info.T.RT];
data.correct = [Info.T.ResponseCorrect];

data.response = zeros(1,n_trials);
data.response(data.strongDirection(1:n_trials)==1 & data.correct(1:n_trials)==1) = 1;
data.response(data.strongDirection(1:n_trials)==1 & data.correct(1:n_trials)==0) = 2;
data.response(data.strongDirection(1:n_trials)==2 & data.correct(1:n_trials)==1) = 2;
data.response(data.strongDirection(1:n_trials)==2 & data.correct(1:n_trials)==0) = 1;

% exclude trials. more exclusion criteria could be added here.
ex = zeros(1,n_trials);
if c
    %ex(isnan(data.conf)|data.RT>=8|data.RT <= 0.3) = 1;
    ex(isnan(data.conf)|data.RT <= 0.3) = 1;
    skipped_trials = sum(isnan(data.conf))/length(data.conf)
    RT_excl = sum(data.RT <= 0.3)/length(data.conf)
end   % skipped trials, and too quick or too long RTs

data.signalDiffs     = data.signalDiffs(~ex);
data.strongDirection = data.strongDirection(~ex);
if c, data.conf      = data.conf(~ex); end
data.RT              = data.RT(~ex);
data.correct         = data.correct(~ex);
data.response        = data.response(~ex);
if c
    data.excl_skipped    = skipped_trials;
    data.excl_RT         = RT_excl;
end

end